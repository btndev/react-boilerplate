# Bitnoise react boilerplate

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
To find our what it has to offer go to [User Guide](https://github.com/facebookincubator/create-react-app/blob/v1.0.13/packages/react-scripts/template/README.md).

## Development

### Create local environment config file from dist

`cp .env.local.dist .env.local`

### Install dependencies

`yarn install`

### Start development server via [react-scripts](https://github.com/facebookincubator/create-react-app/blob/v1.0.13/packages/react-scripts/template/README.md)

`yarn dev`

### Generate new component based on existing components via [generact](https://github.com/diegohaz)

`yarn component`

### Suggested Chrome extensions for development

* [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi) - Adds React debugging tools to the Chrome Developer Tools. 
* [Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd) - Redux DevTools for debugging application's state changes.
* [Immutable.js Object Formatter](https://chrome.google.com/webstore/detail/immutablejs-object-format/hgldghadipiblonfkkicmgcbbijnpeog) - Transforms Immutable JS objects to a more readable format when they are logged to the console. 
* [React-Sight](https://chrome.google.com/webstore/detail/react-sight/aalppolilappfakpmdfdkpppdnhpgifn) - Extends the Developer Tools, adding a sidebar that displays React Component Hierarchy.

## What's inside ?

If You not unfamiliar with some of this projects I suggest to go thru their docs first.

* [axios](https://github.com/mzabriskie/axios) - Promise based HTTP client for the browser and node.js
* [bootstrap](https://github.com/twbs/bootstrap) - The most popular HTML, CSS, and JavaScript framework for developing responsive, mobile first projects on the web.
* [font-awesome](https://github.com/FortAwesome/Font-Awesome) - The iconic font and CSS toolkit
* [immutable](https://facebook.github.io/immutable-js/) - Immutable collections for JavaScript
* [halogen](https://github.com/yuanyan/halogen) - A collection of loading spinners with React.js
* [lodash](https://lodash.com/) - A modern JavaScript utility library delivering modularity, performance & extras.
* [react](https://facebook.github.io/react/) - A JavaScript library for building user interfaces
* [react-breadcrumbs-dynamic](https://github.com/oklas/react-breadcrumbs-dynamic) - Breadcrumbs extremely flexible and easy to use
* [react-fontawesome](https://github.com/danawoodman/react-fontawesome) - A React Font Awesome component.
* [react-helmet](https://github.com/nfl/react-helmet) - A document head manager for React
* [react-immutable-proptypes](https://github.com/HurricaneJames/react-immutable-proptypes) - PropType validators that work with Immutable.js.
* [react-loadable](https://github.com/thejameskyle/react-loadable) - A higher order component for loading components with promises.
* [react-portal](https://github.com/tajo/react-portal) - React component for transportation of modals, lightboxes, loading bars... to document.body
* [react-redux](https://github.com/reactjs/react-redux) - Official React bindings for Redux
* [react-redux-loading-bar](https://github.com/mironov/react-redux-loading-bar) - Loading Bar (aka Progress Bar) for Redux and React
* [react-redux-toastr](https://github.com/diegoddox/react-redux-toastr) - Toastr message implemented with Redux
* [react-router](https://reacttraining.com/react-router/) - Declarative routing for React
* [react-router-redux](https://github.com/reactjs/react-router-redux) - Ruthlessly simple bindings to keep react-router and redux in sync
* [reactstrap](https://github.com/reactstrap/reactstrap) - Simple React Bootstrap 4 components
* [redux](http://redux.js.org/) - Redux is a predictable state container for JavaScript apps.
* [redux-auth-wrapper](https://github.com/mjrussell/redux-auth-wrapper) - A React Higher Order Component (HOC) for handling Authentication and Authorization with Routing and Redux
* [redux-axios-middleware](https://github.com/svrcekmichal/redux-axios-middleware) - Redux middleware for fetching data with axios HTTP client
* [redux-form](http://redux-form.com/7.0.4/) - A Higher Order Component using react-redux to keep form state in a Redux store
* [redux-saga](https://redux-saga.js.org/) - An alternative side effect model for Redux apps
* [redux-thunk](https://github.com/gaearon/redux-thunk) - Thunk middleware for Redux
* [reselect](https://github.com/reactjs/reselect) - Selector library for Redux
* [styled-components](https://www.styled-components.com/) - Visual primitives for the component age.
* [generact](https://github.com/diegohaz/generact) - Tool for generating React components by replicating your own

## Folder structure

Inspired by [Three Rules For Structuring (Redux) Applications](https://jaysoo.ca/2016/02/28/organizing-redux-application/)    

[Read more](docs/FolderStructure.md) about folder structure.

## File/Folder naming convention

* If file exports `function`/`class` its name should match
* If folder exports `component`/`container` its name should match `component`/`container`
* Other files should follow [lowerCamelCase](https://en.wiktionary.org/wiki/lowerCamelCase)

## Documentation

[Read more](docs/README.md).

## Remark

This is just suggested set of tools/solutions/concepts. It can be overkill for some smaller projects. Use with consideration and common sense.

## What is required to follow

[Folder structure](docs/FolderStructure.md) for consistency between company projects.
`react`, `redux`, `react-router`.

## What is optional

All other libraries/concepts can be simplified/replaced with some counterparts based on developer skill set, knowledge, size of project.
