FROM nginx:1.13.12-alpine

COPY ./build /app

COPY ./.nginx.conf /etc/nginx/conf.d/default.conf
