module.exports = {
  root: true,
  extends: [
    'eslint-config-bitnoise-react',
  ],
  plugins: [
  ],
  rules: {
  },
  settings: {
    'import/resolver': {
      node: {
        'moduleDirectory': [
          'node_modules',
          'src',
        ],
      },
    },
  },
}
