# Page

## What is Page ?

* Page is entry point for [Route](https://reacttraining.com/react-router/web/api/Router) ([react-router](https://github.com/ReactTraining/react-router))
* Page can be React Component, Redux Container, [asyncPage](./Api.md#asyncpage).
* Page should be wrapped using [connectPage](./Api.md#connectpage) HOC.
* Page components/containers should have `Page` suffix to distinct them from other `components`/`containers`

## Api reference

[Read more](./Api.md#page-module)

## Examples of most common types of pages

* [CounterPage](../src/pages/CounterPage) - simple example of page concept with simple reducer, and actions with comments
* [SecurePage](../src/pages/SecurePage) - example of page with restriction for authenticated users only
* [UsersPage](../src/pages/UsersPage) - example of page with nested pages inside
* [ListUsersPage](../src/pages/UsersPage/ListUsersPage) - example of page that fetch data using routine, saga
* [CreateUserPage](../src/pages/UsersPage/CreateUserPage) - example of create user entity using single routine, saga, form
* [EditUserPage](../src/pages/UsersPage/EditUserPage) - more complete example using multiple routines, sagas, form
