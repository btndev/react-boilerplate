# Folder structure

Inspired by [Three Rules For Structuring (Redux) Applications](https://jaysoo.ca/2016/02/28/organizing-redux-application/)

```
public/ - Public folder
src/
    actions/ - Redux actions
    components/ - React components
        <COMPONENT_NAME>
            index.js - React component
    containers/ - Redux containers
        <CONTAINER_NAME>
            index.js - Redux container
            actions.js
            constants.js
            reducer.js
            sagas.js
            selectors.js
            routines.js
    forms/ - Redux-form components
    hoc/ - Higher-Order components
    layouts/ - Layout components for Pages
    middleware/ - Redux middleware
    modules/ - Entities grouped by common logical denominator
        <MODULE_NAME>/
            components/ - React components
                <COMPONENT_NAME>
                    index.js - React component
            containers/ - Redux container
                <CONTAINER_NAME>
                    index.js - Redux container
                    actions.js
                    constants.js
                    reducer.js
                    sagas.js
                    selectors.js
            forms/ - Redux-form components
            reducer.js - Composition of containers reducers in module
            sagas.js - Composition of containers sagas in module
            utils.js - utils in namespace
            ...
    pages/
        <PAGE_NAME>Page/
            index.js - Redux component/container
            actions.js - Page specific actions
            constants.js - Page specific constants
            reducer.js - Page specific reducer
            saga.js - Page specific saga
            selectors.js - Page specific selectors
            routines.js - Page specific routines
    reducers/ - Redux reducers
    sagas/ - Redux-sagas
    services/ - Services for comunication/storage etc.
    store/ - Redux store configuration/creation.
    styles/ - Styled components
         globalStyles.js - Global styles for html/body
         variables.js - Variables for styled components
         mixins.js - Mixins for styled components
    utils/ - Utility function (helper functions)
    App.js - App React component
    Router.js - Router React component
```

Elements like `components`, `containers`, `sagas`, `constants`, `sagas`, `selectors`, `reducer`, `routines`, `mixins` can be either single file or folder with index.js and other files (depending on complexity of specific entity). They can be either global (in `src/` folder) or grouped in`modules`. 

# What is `hoc(Higher-Order Components)` ?

> A higher-order component (HOC) is an advanced technique in React for reusing component logic. HOCs are not part of the React API, per se. They are a pattern that emerges from React’s compositional nature.

[Read more](https://reactjs.org/docs/higher-order-components.html)

# What is `layout` ?

Layout is general page layout for [Pages](Page.md). 

# What is `module` ?

Module is common denominator for elements. Think about it like feature or [Namespace](https://en.wikipedia.org/wiki/Namespace). 

# What is `routine` ?

> A smart action creator for [Redux](https://github.com/reactjs/redux). Useful for any kind of async actions like fetching data.
> Also fully compatible with [Redux Saga](https://github.com/yelouafi/redux-saga) and [Redux Form](https://github.com/erikras/redux-form).

> Routine is a smart action creator that encapsulates 5 action types and 5 action creators to make standard actions lifecycle easy-to-use: TRIGGER -> REQUEST -> SUCCESS / FAILURE -> FULFILL

[Read more](https://github.com/afitiskin/redux-saga-routines)

# What is `page` ?

Page is entry point for [Route](https://reacttraining.com/react-router/web/api/Router) ([react-router](https://github.com/ReactTraining/react-router))

[Read more](Page.md)
