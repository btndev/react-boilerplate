# Documentation 

Documentation is generated using [documentation.js](http://documentation.js.org/).

## Markdown

[Docs](./Api.md)

## Html

`yarn install`

`yarn serve`

[Docs](http://localhost:4001)

## Development


### Install dependencies

`yarn install`

### Serve documentation (`HTML`)

`yarn serve`

### Generate documentation (`HTML`)

`yarn build:html`

### Generate documentation (`Markdown`)

`yarn build:md`
