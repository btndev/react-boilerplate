import { storage } from 'store'

/**
 * Get item from local storage.
 *
 * @param {string} key - Key you want to get.
 *
 * @returns {*} - Value for key.
 */
function getItem (key) {
  return storage.read(key)
}

/**
 * Set item in local storage.
 *
 * @param {string} key - Key you want to create/update.
 * @param {*} value - Value you want to create/update.
 */
function setItem (key, value) {
  storage.write(key, value)
}

/**
 * Remove item from local storage.
 *
 * @param {string} key - Key you want to remove.
 */
function removeItem (key) {
  storage.remove(key)
}

const localStorage = {
  getItem,
  setItem,
  removeItem,
}

export default localStorage
