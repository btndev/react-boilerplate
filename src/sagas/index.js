import { routinePromiseWatcherSaga } from 'redux-saga-routines'
import authSagas from 'modules/auth/sagas'
// import you global sagas here

const sagas = [
  routinePromiseWatcherSaga,
  ...authSagas,
  // put your global sagas here for saga middleware
]

export default sagas
