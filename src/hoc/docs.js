/**
 * React component, stateless function or ES6 Class
 *
 * @typedef {Function|Class} Component
 */

/**
 * Component wrapped with Higher-Order Component.
 *
 * @typedef {Component} WrappedComponent
 */

/**
 * Component wrapper function returned from Higher-Order Components.
 *
 * @typedef {Function} ComponentWrapperFunction
 * @param {Component} Component to wrap
 *
 * @returns {WrappedComponent} Wrapped component
 */
