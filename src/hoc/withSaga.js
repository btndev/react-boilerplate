import React, { Component } from 'react'
import PropTypes from 'prop-types'
import hoistNonReactStatics from 'hoist-non-react-statics'
import wrapDisplayName from 'recompose/wrapDisplayName'

/**
 * Higher-Order Component that will inject saga using {@link injectSagaEnhancer} on component mount and eject on component unmount.
 *
 * @param {Object} options - Options.
 * @param {string} options.key - Unique key that will be used to identify saga.
 * @param {Generator} options.saga - Saga generator function.
 *
 * @returns {ComponentWrapperFunction} - Component wrapper function.
 */
function withSaga ({ key, saga }) {
  return (WrappedComponent) => {
    class WithSaga extends Component {
      componentWillMount () {
        const { store } = this.context

        store.injectSaga(key, saga)
      }

      componentWillUnmount () {
        const { store } = this.context

        store.ejectSaga(key)
      }

      render () {
        return (<WrappedComponent {...this.props} />)
      }
    }

    WithSaga.displayName = wrapDisplayName(WrappedComponent, 'withSaga')

    WithSaga.contextTypes = {
      store: PropTypes.object.isRequired,
    }

    return hoistNonReactStatics(WithSaga, WrappedComponent)
  }
}

export default withSaga
