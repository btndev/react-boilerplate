import React from 'react'
import hoistNonReactStatics from 'hoist-non-react-statics'
import wrapDisplayName from 'recompose/wrapDisplayName'
import Loading from 'components/Loading'

function shouldShowLoading (props, { prop, eager = false }) {
  if (!eager && !Reflect.has(props, prop)) {
    return false
  }

  return !props[prop]
}

/**
 * Higher-Order Component that will prevent component from rendering when data it requires is still loading.
 *
 * @param {Object} options - Options.
 * @param {string} options.prop - Prop that should be used for data check.
 * @param {?Boolean} options.eager - Define if missing prop (undefined) should be treated as loading state.
 *
 * @returns {ComponentWrapperFunction} - Component wrapper function.
 */
function withLoading (options) {
  return (WrappedComponent) => {
    function WithLoading (props) {
      if (shouldShowLoading(props, options)) {
        return (<Loading />)
      }

      return (<WrappedComponent {...props} />)
    }

    WithLoading.displayName = wrapDisplayName(WrappedComponent, 'withLoading')

    return hoistNonReactStatics(WithLoading, WrappedComponent)
  }
}

export default withLoading
