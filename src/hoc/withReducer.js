import React, { Component } from 'react'
import PropTypes from 'prop-types'
import hoistNonReactStatics from 'hoist-non-react-statics'
import wrapDisplayName from 'recompose/wrapDisplayName'

/**
 * Higher-Order Component that will inject reducer using {@link injectReducerEnhancer} on component mount.
 *
 * @param {Object} options - Options.
 * @param {string} options.key - State subset key that reducer will handle in global Redux state.
 * @param {Function} options.reducer - Reducer function that will handle state subset under state key.
 *
 * @returns {ComponentWrapperFunction} - Component wrapper function.
 */
function withReducer ({ key, reducer }) {
  return (WrappedComponent) => {
    class WithReducer extends Component {
      componentWillMount () {
        const { store } = this.context

        store.injectReducer(key, reducer)
      }

      render () {
        return (<WrappedComponent {...this.props} />)
      }
    }

    WithReducer.displayName = wrapDisplayName(WrappedComponent, 'withReducer')

    WithReducer.contextTypes = {
      store: PropTypes.object.isRequired,
    }

    return hoistNonReactStatics(WithReducer, WrappedComponent)
  }
}

export default withReducer
