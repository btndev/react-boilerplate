import loadable from 'react-loadable'
import LoadingComponent from 'components/LoadingComponent'

/**
 * Higher-Order Component that will lazy load other component using {@link https://github.com/thejameskyle/react-loadable|react-loadable} to utilize webpack {@link https://webpack.js.org/guides/code-splitting/|code splitting}.
 *
 * @see {@link https://webpack.js.org/guides/code-splitting/}
 * @see {@link https://github.com/thejameskyle/react-loadable}
 *
 * @param {Object} options - {@link https://github.com/thejameskyle/react-loadable}.
 *
 * @returns {WrappedComponent} Wrapped component.
 */
function asyncComponent (options) {
  return loadable({
    loading: LoadingComponent,
    ...options,
  })
}

export default asyncComponent
