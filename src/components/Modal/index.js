import React from 'react'
import PropTypes from 'prop-types'
import {
  Modal as BaseModal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap'

function Modal ({
  header,
  children: body,
  footer,
  enableEscape,
  ...restProps
}) {
  const backdrop = enableEscape || 'static'
  const keyboard = enableEscape

  return (
    <BaseModal keyboard={keyboard} backdrop={backdrop} {...restProps}>
      {
        header &&
          <ModalHeader toggle={restProps.toggle}>
            {header}
          </ModalHeader>
      }
      {
        body &&
          <ModalBody>
            {body}
          </ModalBody>
      }
      {
        footer &&
          <ModalFooter>
            {footer}
          </ModalFooter>
      }
    </BaseModal>
  )
}

Modal.propTypes = {
  header: PropTypes.node,
  children: PropTypes.node,
  footer: PropTypes.node,
  isOpen: PropTypes.bool,
  onOpened: PropTypes.func,
  onClosed: PropTypes.func,
  enableEscape: PropTypes.bool,
  toggle: PropTypes.func,
}

export default Modal
