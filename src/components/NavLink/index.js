import React from 'react'
import StyledNavLink from './StyledNavLink'

function NavLink (props) {
  return (
    <StyledNavLink activeClassName="active" {...props} />
  )
}

export default NavLink
