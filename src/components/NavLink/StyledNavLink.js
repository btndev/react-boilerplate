import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

import {
  defaultLinkColor,
  activeLinkColor,
} from 'styles/variables'

const StyledNavLink = styled(NavLink)`
  color: ${defaultLinkColor};

  &.active {
    color: ${activeLinkColor};
  }
`

export default StyledNavLink
