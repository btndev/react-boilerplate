import React from 'react'
import PropTypes from 'prop-types'
import Loading from 'components/Loading'
import Error from 'components/Error'

function LoadingComponent (props) {
  if (props.isLoading) {
    // While our other component is loading...
    if (props.timedOut) {
      // In case we've timed out loading our other component.
      return (<Error>Loader timed out!</Error>)
    } else if (props.pastDelay) {
      // Display a loading screen after a set delay.
      return (<Loading />)
    }

    // Don't flash "Loading..." when we don't need to.
    return null
  } else if (props.error) {
    // If we aren't loading, maybe
    return (<Error>Error! Component failed to load</Error>)
  }

  // This case shouldn't happen... but we'll return null anyways.
  return null
}

LoadingComponent.propTypes = {
  error: PropTypes.bool,
  pastDelay: PropTypes.bool,
  timedOut: PropTypes.bool,
  isLoading: PropTypes.bool,
}

export default LoadingComponent
