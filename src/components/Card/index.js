import React from 'react'
import PropTypes from 'prop-types'
import {
  CardHeader,
  Card as BaseCard,
  CardBody,
} from 'reactstrap'

function Card ({ header, children }) {
  return (
    <BaseCard>
      {header && <CardHeader>{header}</CardHeader>}
      {children && <CardBody>{children}</CardBody>}
    </BaseCard>
  )
}

Card.propTypes = {
  header: PropTypes.node,
  children: PropTypes.node,
}

export default Card
