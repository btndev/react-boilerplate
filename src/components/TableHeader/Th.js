import styled from 'styled-components'

const Th = styled.th`
  ${(props) => (props.right && 'text-align: right;')}
`

export default Th
