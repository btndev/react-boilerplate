import React from 'react'
import PropTypes from 'prop-types'

import Th from './Th'

function TableHeader ({ children, right }) {
  return (
    <Th scope="col" right={right}>
      {children}
    </Th>
  )
}

TableHeader.propTypes = {
  children: PropTypes.node,
  right: PropTypes.bool,
}

export default TableHeader
