import React from 'react'
import uuid from 'uuid'
import pure from 'recompose/pure'
import PropTypes from 'prop-types'
import Portal from 'components/Portal'
import Spinner from 'react-spinkit'
import Tooltip from 'components/Tooltip'

import Wrapper from './Wrapper'

function Loading ({ children }) {
  const id = `loading-${uuid()}`

  return (
    <Portal isOpened>
      <Wrapper>
        <Spinner name="ball-beat" color="#333" id={id} />
        <Tooltip placement="bottom" target={id}>
          {children || 'Loading'}
        </Tooltip>
      </Wrapper>
    </Portal>
  )
}

Loading.propTypes = {
  children: PropTypes.string,
}

export default pure(Loading)
