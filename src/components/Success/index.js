import React from 'react'
import PropTypes from 'prop-types'
import Feedback from 'components/Feedback'

function Success ({ children, alert }) {
  if (!children) {
    return null
  }

  return (
    <Feedback type="success" alert={alert}>
      {children}
    </Feedback>
  )
}

Success.propTypes = {
  alert: PropTypes.bool,
  children: PropTypes.node,
}

export default Success
