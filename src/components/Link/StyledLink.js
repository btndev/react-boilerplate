import { Link } from 'react-router-dom'
import styled from 'styled-components'

import { defaultLinkColor } from 'styles/variables'

const StyledLink = styled(Link)`
  color: ${defaultLinkColor};
`

export default StyledLink
