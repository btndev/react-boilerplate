import styled from 'styled-components'
import PropTypes from 'prop-types'

const Td = styled.td`
  ${(props) => (props.right && 'text-align: right;')}
  ${(props) => (props.middle && 'vertical-align: middle !important;')}
`

Td.propTypes = {
  right: PropTypes.bool,
  middle: PropTypes.bool,
}

export default Td
