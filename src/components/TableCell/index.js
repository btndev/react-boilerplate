import React from 'react'
import PropTypes from 'prop-types'
import Td from './Td'

function TableCell ({ children, right, middle }) {
  return (
    <Td right={right} middle={middle}>
      {children}
    </Td>
  )
}

TableCell.propTypes = {
  children: PropTypes.node,
  right: PropTypes.bool,
  middle: PropTypes.bool,
}

export default TableCell
