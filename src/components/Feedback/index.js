import React from 'react'
import pure from 'recompose/pure'
import PropTypes from 'prop-types'
import Alert from 'components/Alert'
import Toast from 'components/Toast'

const types = [
  'success',
  'error',
  'info',
]

const alertColorMap = {
  info: 'info',
  success: 'success',
  error: 'danger',
}

function Feedback ({ type, children, alert }) {
  if (!children) {
    return null
  }

  if (alert) {
    return (
      <Alert color={alertColorMap[type]}>{children}</Alert>
    )
  }

  return (
    <Toast type={type}>
      {children}
    </Toast>
  )
}

Feedback.propTypes = {
  alert: PropTypes.bool,
  children: PropTypes.node,
  type: PropTypes.oneOf(types),
}

export default pure(Feedback)
