import React from 'react'
import PropTypes from 'prop-types'

import Wrapper from './Wrapper'
import Inner from './Inner'

function Middle ({ children }) {
  return (
    <Wrapper>
      <Inner>
        {children}
      </Inner>
    </Wrapper>
  )
}

Middle.propTypes = {
  children: PropTypes.node,
}

export default Middle
