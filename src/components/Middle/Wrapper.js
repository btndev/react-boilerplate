// import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  flex-grow: 1;
  width: 100%;
  display: flex;
  flex-direction: row;
  align-content: center;
  justify-content: center;
`

export default Wrapper
