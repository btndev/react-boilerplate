import React, { Children } from 'react'
import PropTypes from 'prop-types'
import Loading from 'components/Loading'

const isLoaded = (data) => (!!data)

function LoadingData ({ data, children }) {
  if (!isLoaded(data)) {
    return (<Loading />)
  }

  return Children.only(children)
}

LoadingData.propTypes = {
  data: PropTypes.any, // eslint-disable-line react/forbid-prop-types
  children: PropTypes.node,
}

export default LoadingData
