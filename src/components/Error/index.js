import React from 'react'
import PropTypes from 'prop-types'
import Feedback from 'components/Feedback'

function Error ({ children, alert }) {
  if (!children) {
    return null
  }

  return (
    <Feedback type="error" alert={alert}>
      {children}
    </Feedback>
  )
}

Error.propTypes = {
  alert: PropTypes.bool,
  children: PropTypes.node,
}

export default Error
