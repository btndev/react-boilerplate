import React from 'react'
import PropTypes from 'prop-types'
import Button from 'components/Button'
import Modal from 'components/Modal'
import { renderInlineButtons } from 'utils/render'

function Confirm ({
  title,
  message,
  onConfirm,
  onCancel,
  confirmLabel,
  confirmColor,
  cancelLabel,
  cancelColor,
  enableEscape,
  handleClose,
}) {
  const footer = renderInlineButtons([
    <Button key="cancel" color={confirmColor} onClick={onConfirm}>{confirmLabel}</Button>,
    <Button key="ok" color={cancelColor} onClick={onCancel}>{cancelLabel}</Button>,
  ])

  return (
    <Modal
      isOpen
      toggle={handleClose}
      enableEscape={enableEscape}
      header={title}
      footer={footer}
    >
      {message}
    </Modal>
  )
}

Confirm.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string.isRequired,
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  confirmLabel: PropTypes.string,
  confirmColor: PropTypes.string,
  cancelLabel: PropTypes.string,
  cancelColor: PropTypes.string,
  enableEscape: PropTypes.bool,
  handleClose: PropTypes.func,
}

Confirm.defaultProps = {
  title: 'Are You Sure?',
  confirmLabel: 'OK',
  confirmColor: 'primary',
  cancelLabel: 'Cancel',
  cancelColor: 'link',
  enableEscape: true,
}

export default Confirm
