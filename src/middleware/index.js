import { loadingBarMiddleware } from 'react-redux-loading-bar'
import resetFormErrorMiddleware from 'modules/form/middleware/resetFormErrorMiddleware'
import resetFormSubmitSucceededMiddleware from 'modules/form/middleware/resetFormSubmitSucceededMiddleware'
import toastMiddleware from 'modules/toast/middleware/toastMiddleware'
// import your middleware here

const middleware = [
  loadingBarMiddleware({
    // redux-saga-routines suffixes
    promiseTypeSuffixes: ['REQUEST', 'SUCCESS', 'FAILURE'],
  }),
  resetFormErrorMiddleware,
  resetFormSubmitSucceededMiddleware,
  toastMiddleware,
  // add your middleware here
]

export default middleware
