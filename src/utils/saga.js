/* eslint-disable import/prefer-default-export */
import { delay } from 'redux-saga'
import { all, call } from 'redux-saga/effects'

/**
 * Add Minimum delay to saga effect.
 *
 * @param {Generator|CallEffect} effect - Saga effect generator.
 * @param {number} minDelay - Min delay to wait to resolve result from effect generator.
 * @returns {*} - Result from generator.
 */
function * withMinDelay (effect, minDelay = 300) {
  const { result } = yield all({
    result: effect,
    delay: call(delay, minDelay),
  })

  return result
}

export {
  withMinDelay,
}
