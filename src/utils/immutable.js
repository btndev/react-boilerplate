import { Set } from 'immutable'

function keyIn (...keys) {
  const keySet = Set(keys)

  return (value, key) => keySet.has(key)
}

function pick (data, ...keys) {
  return data.filter(keyIn(...keys))
}

function omit (data, ...keys) {
  return data.filterNot(keyIn(...keys))
}

export {
  keyIn,
  pick,
  omit,
}
