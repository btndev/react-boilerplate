/* eslint-disable import/prefer-default-export */
import { parse as baseParse } from 'qs'

function parse (search) {
  return baseParse(search, { ignoreQueryPrefix: true })
}

export {
  parse,
}
