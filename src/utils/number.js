/* eslint-disable import/prefer-default-export */
import toInteger from 'lodash/toInteger'

export {
  toInteger,
}
