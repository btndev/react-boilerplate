import { delay } from 'redux-saga'
import { all, call } from 'redux-saga/effects'
import { withMinDelay } from './saga'

describe('withDelay', () => {
  it('should add min delay to effect call', () => {
    const sampleEffect = jest.fn()

    const minDelay = 500
    const testEffect = withMinDelay(sampleEffect, minDelay)

    expect(testEffect.next()).toMatchObject({
      value: all({
        result: sampleEffect,
        delay: call(delay, minDelay),
      }),
    })

    expect(testEffect.next({
      result: 'result',
      delay: true,
    })).toMatchObject({
      value: 'result',
      done: true,
    })
  })
})
