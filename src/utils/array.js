/* eslint-disable import/prefer-default-export */

function injectBetweenItems (array, inject) {
  return array.reduce((acc, value, index) => {
    if (index === 0 || index === array.length) {
      return [...acc, value]
    }

    return [...acc, inject, value]
  }, [])
}

export {
  injectBetweenItems,
}
