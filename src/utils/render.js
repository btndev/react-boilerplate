/* eslint-disable import/prefer-default-export */
import { injectBetweenItems } from 'utils/array'

function renderInlineButtons (buttons) {
  if (!Array.isArray(buttons)) {
    return buttons
  }

  return injectBetweenItems(buttons, ' ')
}

export {
  renderInlineButtons,
}
