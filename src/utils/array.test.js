import { injectBetweenItems } from './array'

describe('injectBetweenItems', () => {
  it('should inject between items', () => {
    const item1 = jest.fn()
    const item2 = jest.fn()
    const item3 = jest.fn()

    const inject = ' '

    expect(injectBetweenItems([
      item1,
      item2,
      item3,
    ], inject)).toEqual([
      item1,
      inject,
      item2,
      inject,
      item3,
    ])
  })

  it('should leave single element array unchanged', () => {
    const item = jest.fn()

    const inject = ' '

    expect(injectBetweenItems([item], inject)).toEqual([item])
  })
})
