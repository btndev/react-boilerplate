/* eslint-disable import/prefer-default-export */
import { toInteger } from 'utils/number'

function toPx (value) {
  return `${toInteger(value)}px`
}

export {
  toPx,
}
