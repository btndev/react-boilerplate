
export const defaultLinkColor = 'blue'
export const activeLinkColor = 'red'

export const footerHeight = '60px'

export const gutterWidth = '30px'
