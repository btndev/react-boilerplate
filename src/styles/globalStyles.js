import 'bootstrap/dist/css/bootstrap.css'
import { injectGlobal } from 'styled-components'

injectGlobal`
  html {
    position: relative;
    min-height: 100%;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  }

  body,
  html {
    margin: 0;
    padding: 0;
  }
  
  body {
    background-color: #fafafa;
    line-height: 1.4;
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    color: #333;
  }
`
