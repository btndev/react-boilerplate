import { css } from 'styled-components'

const clickable = css`
  cursor: pointer;
`

const noWarp = css`
  white-space: nowrap;
`

const useWhiteSpaces = css`
  white-space: pre-wrap;
`

const truncate = css`
  ${noWarp}
  overflow: hidden;
  text-overflow: ellipsis;
`

const breakWords = css`
  overflow-wrap: break-word;
  word-wrap: break-word;
  -ms-word-break: break-all;
  word-break: break-word;
  hyphens: auto;
`

const pullRight = css`
  margin-left: auto;
`

const onClickCursor = (props) => (
  `cursor: ${props.onClick ? 'pointer' : 'auto'};`
)

const responsiveImage = css`
  max-width: 100%;
  height: auto;
  display: block;
`

const noPrint = css`
  @media print {
    display: none !important;
  }
`

export {
  clickable,
  noWarp,
  useWhiteSpaces,
  truncate,
  breakWords,
  pullRight,
  onClickCursor,
  responsiveImage,
  noPrint,
}
