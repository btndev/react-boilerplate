import React from 'react'
import ReactDOM from 'react-dom'
import createHistory from 'history/createBrowserHistory'
import { Provider } from 'react-redux'
import { BreadcrumbsProvider } from 'react-breadcrumbs-dynamic'
import { ConnectedRouter } from 'react-router-redux'
import { configureStore } from './store'
import App from './App'
import registerServiceWorker from './registerServiceWorker'

import './styles/globalStyles'

const MOUNT_NODE = document.getElementById('root')

const history = createHistory()
const initialState = {}
const store = configureStore(initialState, { history })

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <BreadcrumbsProvider>
          <App />
        </BreadcrumbsProvider>
      </ConnectedRouter>
    </Provider>,
    MOUNT_NODE
  )
}

render()
registerServiceWorker()
