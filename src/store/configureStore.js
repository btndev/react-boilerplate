/* eslint-disable import/max-dependencies */
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'
import { routerMiddleware } from 'react-router-redux'
import { fromJS } from 'immutable'
import createSagaMiddleware from 'redux-saga'
import thunkMiddleware from 'redux-thunk'
import axiosMiddleware from 'redux-axios-middleware'
import api from 'services/api'
import sagas from 'sagas'
import middleware from 'middleware'

import createReducer from './createReducer'
import createInjectReducerEnhancer from './createInjectReducerEnhancer'
import createInjectSagaEnhancer from './createInjectSagaEnhancer'

/**
 * Configure Redux store.
 *
 * @param {*} initialState - Store Initial state.
 * @param {Object} options - Additional options.
 * @param {Object} options.history - React router history.
 *
 * @returns {ReduxStore} - Redux store.
 */
function configureStore (initialState = {}, { history }) {
  const sagaMiddleware = createSagaMiddleware()

  const middlewares = [
    thunkMiddleware,
    sagaMiddleware,
    axiosMiddleware(api),
    routerMiddleware(history),
    ...middleware,
  ]

  const enhancers = [
    createInjectReducerEnhancer({ createReducer }),
    createInjectSagaEnhancer({ sagaMiddleware }),
    applyMiddleware(...middlewares),
  ]

  const reducer = createReducer()
  const preloadedState = fromJS(initialState)
  const enhancer = composeWithDevTools(...enhancers)

  const store = createStore(reducer, preloadedState, enhancer)

  const sagasToRun = [
    ...sagas,
  ].filter(Boolean)

  sagasToRun.forEach((saga) => {
    sagaMiddleware.run(saga)
  })

  return store
}

export default configureStore
