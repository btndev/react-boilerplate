/* eslint-disable import/prefer-default-export */
import configureStore from './configureStore'

export {
  configureStore,
}
