
function createInjectReducerEnhancer ({ createReducer }) {
  /**
   * Redux store enhancer with inject reducer functionality.
   *
   * @name injectReducerEnhancer
   * @param {Function} createStore - Create store function.
   *
   * @returns {ReduxStore} - Enhanced Redux store.
   */
  return (createStore) => (...args) => {
    const store = createStore(...args)

    store.injectedReducers = {}

    store.injectReducer = (key, reducer) => {
      if (Reflect.has(store.injectedReducers, key) && store.injectedReducers[key] === reducer) {
        return
      }

      if (process.env.NODE_ENV === 'development') {
        const { default: getDisplayName } = require('recompose/getDisplayName') // eslint-disable-line global-require
        console.info(`injecting reducer: ${key} => ${getDisplayName(reducer)}`) // eslint-disable-line no-console
      }

      store.injectedReducers[key] = reducer
      store.replaceReducer(createReducer(store.injectedReducers))
    }

    return store
  }
}

export default createInjectReducerEnhancer
