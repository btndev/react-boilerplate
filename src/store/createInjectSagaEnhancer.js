
function createInjectSagaEnhancer ({ sagaMiddleware }) {
  /**
   * Redux store enhancer with inject/eject saga functionality.
   *
   * @name injectSagaEnhancer
   * @param {Function} createStore - Create store function.
   *
   * @returns {ReduxStore} - Enhanced Redux store.
   */
  return (createStore) => (...args) => {
    const store = createStore(...args)

    store.injectedSagas = {}

    store.hasSaga = (key) => (
      Reflect.has(store.injectedSagas, key)
    )

    store.ejectSaga = (key) => {
      if (!store.hasSaga(key)) {
        return
      }

      if (store.injectedSagas[key].instances > 1) {
        store.injectedSagas[key].instances -= 1

        return
      }

      if (process.env.NODE_ENV === 'development') {
        const { default: getDisplayName } = require('recompose/getDisplayName') // eslint-disable-line global-require
        console.info(`ejecting saga: ${key} => ${getDisplayName(store.injectedSagas[key].saga)}`) // eslint-disable-line no-console
      }

      if (store.injectedSagas[key].task) {
        store.injectedSagas[key].task.cancel()
      }

      delete store.injectedSagas[key]
    }

    store.injectSaga = (key, saga, sagaArgs) => {
      if (store.hasSaga(key)) {
        store.injectedSagas[key].instances += 1

        return
      }

      if (process.env.NODE_ENV === 'development') {
        const { default: getDisplayName } = require('recompose/getDisplayName') // eslint-disable-line global-require
        console.info(`injecting saga: ${key} => ${getDisplayName(saga)}`) // eslint-disable-line no-console
      }

      store.injectedSagas[key] = {
        instances: 1,
        saga,
        task: sagaMiddleware.run(saga, sagaArgs),
      }
    }

    return store
  }
}

export default createInjectSagaEnhancer
