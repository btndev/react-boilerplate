/**
 * Redux store
 *
 * @see {@link http://redux.js.org/docs/api/Store.html#store}
 *
 * @typedef {object} ReduxStore
 */
