import { combineReducers } from 'redux-immutable'
import reducers from 'reducers'

function createReducer (injectedReducers) {
  return combineReducers({
    ...reducers,
    ...injectedReducers,
  })
}

export default createReducer
