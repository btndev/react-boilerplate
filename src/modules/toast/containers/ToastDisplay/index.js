import React from 'react'
import ReduxToastr from 'react-redux-toastr'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'

function ToastDisplay () {
  return (
    <ReduxToastr
      timeOut={4000}
      newestOnTop
      preventDuplicates
      position="top-right"
      transitionIn="fadeIn"
      transitionOut="fadeOut"
      progressBar={false}
    />
  )
}

export default ToastDisplay
