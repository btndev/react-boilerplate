import {
  displayToast,
  displaySuccessToast,
  displayErrorToast,
} from 'modules/toast/actions'

function handleToastMeta (toast, store) {
  if (toast.success) {
    store.dispatch(displaySuccessToast(toast.success))
  }

  if (toast.error) {
    store.dispatch(displayErrorToast(toast.error))
  }

  if (toast.type && toast.title) {
    store.dispatch(displayToast(toast))
  }
}

/**
 * Middleware for displaying toasts from redux actions meta.
 *
 * @param {ReduxStore} store - Redux store.
 */
function toastMiddleware (store) {
  return (next) => (action) => {
    if (action.meta && action.meta.toast) {
      handleToastMeta(action.meta.toast, store)
    }

    return next(action)
  }
}

export default toastMiddleware
