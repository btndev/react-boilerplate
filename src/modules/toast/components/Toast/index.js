import { Component } from 'react'
import PropTypes from 'prop-types'
import { toastr } from 'react-redux-toastr'

class Toast extends Component {
  componentDidMount () {
    const { type, children, title } = this.props
    const dispatchToast = toastr[type]

    if (title) {
      dispatchToast(title, children)

      return
    }

    dispatchToast(children)
  }

  shouldComponentUpdate () {
    return false
  }

  render () {
    return null
  }
}

Toast.propTypes = {
  title: PropTypes.string,
  children: PropTypes.string.isRequired,
  type: PropTypes.oneOf([
    'success',
    'info',
    'warning',
    'error',
  ]).isRequired,
}

export default Toast
