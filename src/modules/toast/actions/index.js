/* eslint-disable import/prefer-default-export */
import { actions as toastrActions } from 'react-redux-toastr'

/**
 * Display Toast.
 *
 * @param {Object} options - Toast options.
 * @param {string} options.type - Toast type (success, info, warning, light, error).
 * @param {string} options.title - Toast title.
 * @param {string} options.message - Toast message.
 * @returns {Object} - Redux action.
 */
function displayToast ({ type, title, message }) {
  return toastrActions.add({
    type,
    title,
    message,
  })
}

/**
 * Display success toast.
 *
 * @param {string} title - Toast title.
 * @param {?string} message - Toast message.
 * @returns {Object} - Redux action.
 */
function displaySuccessToast (title, message = undefined) {
  return displayToast({
    type: 'success',
    title,
    message,
  })
}

/**
 * Display error toast.
 *
 * @param {string} title - Toast title.
 * @param {?string} message - Toast message.
 * @returns {Object} - Redux action.
 */
function displayErrorToast (title, message = undefined) {
  return displayToast({
    type: 'error',
    title,
    message,
  })
}

/**
 * Display confirm toast.
 *
 * @param {Object} payload - Confirm toast options.
 * @param {string} payload.message - Confirmation message.
 * @param {Function} payload.onConfirm - On confirm callback.
 * @param {string} payload.onConfirmText - On confirm text.
 * @param {Function} payload.onCancel - On cancel callback.
 * @param {string} payload.onCancelText - On cancel text.
 * @returns {Object} - Redux action.
 */
function displayConfirmToast ({
  message,
  onConfirm,
  onConfirmText,
  onCancel,
  onCancelText,
}) {
  return toastrActions.showConfirm({
    message,
    options: {
      onOk: onConfirm,
      onOkText: onConfirmText,
      onCancel,
      onCancelText,
    },
  })
}

export {
  displayToast,
  displaySuccessToast,
  displayErrorToast,
  displayConfirmToast,
}
