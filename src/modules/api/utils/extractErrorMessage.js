/**
 * Extract Error message from most common error objects.
 *
 * @param {Object|Error|string} error - Error object that message should be extracted from.
 *
 * @returns {string} - Error message extracted from `error`.
 */
function extractErrorMessage (error) {
  if (error.response && error.response.data && error.response.data.error) {
    return error.response.data.error
  }

  if (error.response && error.response.data && error.response.data.message) {
    return error.response.data.message
  }

  if (error.message) {
    return error.message
  }

  return `${error}`
}

export default extractErrorMessage
