import axios from 'axios'
// import { Map } from 'immutable'
import { hasAuthToken, getAuthToken } from 'modules/auth/storage'

const client = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
  // headers: {
  //   'Content-Type': 'application/json',
  // },
  responseType: 'json',
  // transformRequest: [(data) => {
  //   if (Map.isMap(data)) {
  //     return data.toJS()
  //   }
  //
  //   return data
  // }, (data) => {
  //   return JSON.stringify(data)
  // }],
  // transformResponse: [(data) => (data)],
  validateStatus: (status) => (
    status >= 200 && status < 300
  ),
})

const authorizationInterceptor = (config) => {
  if (config.headers && config.headers.Authorization) {
    return config
  }

  if (!hasAuthToken()) {
    return config
  }

  return {
    ...config,
    headers: {
      ...config.headers,
      Authorization: `Bearer ${getAuthToken()}`,
    },
  }
}

client.interceptors.request.use(authorizationInterceptor)

// client.defaults.headers.common['Authorization'] = AUTH_TOKEN
// client.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

export default client
