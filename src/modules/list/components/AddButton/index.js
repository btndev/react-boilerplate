import React from 'react'
import Button from 'components/Button'
import { Link } from 'react-router-dom'

function AddButton (props) {
  return (
    <Button color="success" size="sm" tag={Link} {...props} />
  )
}

AddButton.propTypes = {
  ...Button.propTypes,
}

export default AddButton
