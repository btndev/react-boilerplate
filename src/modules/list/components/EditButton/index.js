import React from 'react'
import PropTypes from 'prop-types'
import Button from 'components/Button'
import { Link } from 'react-router-dom'

function EditButton (props) {
  return (
    <Button color="primary" size="sm" tag={Link} {...props} />
  )
}

EditButton.propTypes = {
  ...Button.propTypes,
  children: PropTypes.string,
}

EditButton.defaultProps = {
  children: 'Edit',
}

export default EditButton
