import React from 'react'
import PropTypes from 'prop-types'
import Button from 'components/Button'
// import { Link } from 'react-router-dom'

function DeleteButton (props) {
  return (
    <Button color="danger" size="sm" {...props} />
  )
}

DeleteButton.propTypes = {
  ...Button.propTypes,
  children: PropTypes.string,
}

DeleteButton.defaultProps = {
  children: 'Delete',
}

export default DeleteButton
