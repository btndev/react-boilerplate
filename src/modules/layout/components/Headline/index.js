import styled from 'styled-components'
import PropTypes from 'prop-types'

const Headline = styled.h2`
  font-size: 1.5em;
  font-weight: 300;
  ${(props) => (props.clearMargin && `margin-bottom: 0;`)}
`

Headline.propTypes = {
  clearMargin: PropTypes.bool,
}

export default Headline
