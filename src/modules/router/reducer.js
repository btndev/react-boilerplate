import { fromJS } from 'immutable'
import { LOCATION_CHANGE } from 'react-router-redux'

const initialState = fromJS({
  location: null,
})

function reducer (state = initialState, action) {
  if (action.type === LOCATION_CHANGE) {
    return state.set('location', action.payload)
  }

  return state
}

export default reducer
