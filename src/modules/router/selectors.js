/* eslint-disable import/prefer-default-export */
import { createSelector } from 'reselect'
import { REDUCER_NAME } from './constants'

const selectRouterState = (state) => (
  state.get(REDUCER_NAME)
)

export const selectLocation = createSelector(
  selectRouterState,
  (state) => state.get('location')
)
