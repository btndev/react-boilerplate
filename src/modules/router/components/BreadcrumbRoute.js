import React from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'
import { BreadcrumbsItem } from 'react-breadcrumbs-dynamic'

function BreadcrumbRoute ({ component: Component, render, ...restProps }) {
  const RenderComponent = (routeProps) =>
    (Component
      ? <Component {...routeProps} />
      : render(routeProps))

  return (
    <Route
      {...restProps}
      render={(routeProps) => ([
        <BreadcrumbsItem key="item" to={restProps.path}>{restProps.label}</BreadcrumbsItem>,
        <RenderComponent key="component" routeProps={routeProps} />,
      ])}
    />
  )
}

BreadcrumbRoute.propTypes = {
  ...Route.propTypes,
  label: PropTypes.node.isRequired,
}

export default BreadcrumbRoute
