import { connect } from 'react-redux'
import compose from 'recompose/compose'
import withSaga from 'hoc/withSaga'

function connectDeleteConfirmButton ({
  key,
  saga,
  payloadProp,
  routine,
}) {
  return (Component) => {
    const mapDispatchToProps = (dispatch, ownProps) => ({
      onConfirm: () => (
        dispatch(routine.trigger(ownProps[payloadProp]))
      ),
    })

    const enhance = compose(
      withSaga({ key, saga }),
      connect(null, mapDispatchToProps)
    )

    return enhance(Component)
  }
}

export default connectDeleteConfirmButton
