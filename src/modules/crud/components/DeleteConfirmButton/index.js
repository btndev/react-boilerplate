import React from 'react'
import PropTypes from 'prop-types'
import Confirm from 'components/Confirm'
import DeleteButton from 'modules/list/components/DeleteButton'

class DeleteConfirmButton extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      isConfirmOpen: false,
    }

    this.showConfirm = this.showConfirm.bind(this)
    this.hideConfirm = this.hideConfirm.bind(this)
    this.handleConfirm = this.handleConfirm.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
  }

  showConfirm () {
    this.setState({ isConfirmOpen: true })
  }

  hideConfirm () {
    this.setState({ isConfirmOpen: false })
  }

  handleConfirm () {
    this.hideConfirm()

    if (this.props.onConfirm) {
      this.props.onConfirm()
    }
  }

  handleCancel () {
    this.hideConfirm()

    if (this.props.onCancel) {
      this.props.onCancel()
    }
  }

  renderConfirm () {
    const { isConfirmOpen } = this.state

    if (!isConfirmOpen) {
      return null
    }

    const { message } = this.props

    return (
      <Confirm
        message={message}
        confirmLabel="Yes, delete it permanently"
        confirmColor="danger"
        cancelColor="link"
        cancelLabel="No"
        handleClose={this.hideConfirm}
        onConfirm={this.handleConfirm}
        onCancel={this.handleCancel}
      />
    )
  }

  render () {
    const RenderConfirm = this.renderConfirm()

    return ([
      <DeleteButton key="button" onClick={this.showConfirm} />,
      <RenderConfirm key="confirm" />,
    ])
  }
}

DeleteConfirmButton.propTypes = {
  message: PropTypes.string,
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
}

DeleteConfirmButton.defaultProps = {
  message: 'Are You sure want to delete?',
}

export default DeleteConfirmButton
