import React from 'react'
import TableCell from 'components/TableCell'

function ListCell (props) {
  return (
    <TableCell middle {...props} />
  )
}

ListCell.propTypes = {
  ...TableCell.propTypes,
}

export default ListCell
