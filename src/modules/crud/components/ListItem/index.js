import React from 'react'
import PropTypes from 'prop-types'
import pure from 'recompose/pure'
import ListCell from 'modules/crud/components/ListCell'
import { renderInlineButtons } from 'utils/render'

function ListItem ({ order, children, actions }) {
  return (
    <tr>
      {
        order &&
          <ListCell>
            {order}
          </ListCell>
      }
      {children}
      {
        actions &&
          <ListCell right>
            {renderInlineButtons(actions)}
          </ListCell>
      }
    </tr>
  )
}

ListItem.propTypes = {
  order: PropTypes.number,
  children: PropTypes.arrayOf(PropTypes.node),
  actions: PropTypes.node,
}

export default pure(ListItem)
