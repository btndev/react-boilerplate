import React, { cloneElement } from 'react'
import PropTypes from 'prop-types'
import Table from 'components/Table'
import TableHeader from 'components/TableHeader'

function List ({ noOrder, noAction, headers, children }) {
  return (
    <Table responsive>
      <thead>
        <tr>
          {!noOrder && <TableHeader>#</TableHeader>}
          {
            headers.map((header, index) => (
              cloneElement(header, { key: index })
            ))
          }
          {!noAction && <TableHeader right>Action</TableHeader>}
        </tr>
      </thead>
      <tbody>
        {children}
      </tbody>
    </Table>
  )
}

List.propTypes = {
  noOrder: PropTypes.bool,
  noAction: PropTypes.bool,
  children: PropTypes.node,
  headers: PropTypes.arrayOf(PropTypes.node).isRequired,
}

export default List
