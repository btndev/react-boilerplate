import {
  REQUIRED,
  FORMAT,
  FORMAT_EMAIL,
  MIN_LENGTH,
  MAX_LENGTH,
} from './constants'

/**
 * Generate user friendly error message from ajv error message object.
 *
 * @param {AjvError} error - Ajv error object.
 *
 * @returns {string} - User friendly error mssage.
 */
function errorMessage (error) {
  if (error.keyword === REQUIRED) {
    return 'Required'
  }

  if (error.keyword === FORMAT && error.params.format === FORMAT_EMAIL) {
    return 'Invalid email address'
  }

  if (error.keyword === MIN_LENGTH) {
    return `Must be ${error.params.limit} or more`
  }

  if (error.keyword === MAX_LENGTH) {
    return `Must be ${error.params.limit} or less`
  }

  return `${error.message}`
}

export default errorMessage
