import createUserFormSchema from '../utils/createUserFormSchema'

const schema = createUserFormSchema({ mode: 'edit' })

export default schema
