import form from 'modules/form/hoc/form'
import BaseUserForm from '../BaseUserForm'
import schema from './schema'
import { FORM_NAME } from './constants'

export default form({
  withLoading: true,
  schema,
  form: FORM_NAME,
})(BaseUserForm)
