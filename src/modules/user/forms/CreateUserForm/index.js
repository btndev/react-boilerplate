import { fromJS } from 'immutable'
import form from 'modules/form/hoc/form'
import BaseUserForm from '../BaseUserForm'
import schema from './schema'
import { FORM_NAME } from './constants'

export default form({
  initialValues: fromJS({}),
  schema,
  form: FORM_NAME,
})(BaseUserForm)
