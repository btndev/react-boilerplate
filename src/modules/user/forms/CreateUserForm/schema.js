import createUserFormSchema from '../utils/createUserFormSchema'

const schema = createUserFormSchema({ mode: 'create' })

export default schema
