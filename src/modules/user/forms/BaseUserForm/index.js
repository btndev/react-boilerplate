import React from 'react'
// import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import Form from 'modules/form/components/Form'
import FormRowField from 'modules/form/components/FormRowField'

function BaseUserForm (props) {
  const { initialValues, handleSubmit } = props

  const createMode = !initialValues.get('id')

  return (
    <Form {...props} onSubmit={handleSubmit} >
      { createMode && <FormRowField type="text" name="username" label="User name" placeholder="Type user name" />}
      { createMode && <FormRowField type="password" name="password" label="Password" placeholder="Type user password" />}
      <FormRowField type="text" name="firstName" label="First Name" placeholder="Type first name" />
      <FormRowField type="text" name="lastName" label="Last Name" placeholder="Type first name" />
    </Form>
  )
}

BaseUserForm.propTypes = {
  ...Form.propTypes,
  initialValues: ImmutablePropTypes.map,
}

export default BaseUserForm
