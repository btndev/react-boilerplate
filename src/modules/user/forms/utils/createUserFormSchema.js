
function createUserFormSchema ({ mode }) {
  const userFormSchema = {
    type: 'object',
    properties: {
      firstName: {
        type: 'string',

      },
      lastName: {
        type: 'string',
      },
    },
    required: ['firstName', 'lastName'],
  }

  if (mode === 'create') {
    userFormSchema.properties = {
      ...userFormSchema.properties,
      username: {
        type: 'string',
        format: 'email',
        minLength: 6,
      },
      password: {
        type: 'string',
        minLength: 6,
      },
    }

    userFormSchema.required = [
      ...userFormSchema.required,
      'username',
      'password',
    ]
  }

  return userFormSchema
}

export default createUserFormSchema
