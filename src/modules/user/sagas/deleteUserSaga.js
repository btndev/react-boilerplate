import { call, put, takeLatest } from 'redux-saga/effects'
import api from 'services/api'
import { withMinDelay } from 'utils/saga'
import extractErrorMessage from 'modules/api/utils/extractErrorMessage'
import { deleteUserRoutine } from 'modules/user/routines'

function * deleteUser ({ payload }) {
  const id = payload.get('id')
  try {
    yield put(deleteUserRoutine.request())
    yield withMinDelay(call(api.delete, `/users/${id}`))
    yield put(deleteUserRoutine.success())
  } catch (error) {
    yield put(deleteUserRoutine.failure(extractErrorMessage(error)))
  } finally {
    yield put(deleteUserRoutine.fulfill())
  }
}

function * deleteUserSaga () {
  yield takeLatest(deleteUserRoutine.TRIGGER, deleteUser)
}

export default deleteUserSaga
