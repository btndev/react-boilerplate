import React from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import DeleteConfirmButton from 'modules/crud/components/DeleteConfirmButton'
import connectDeleteConfirmButton from 'modules/crud/hoc/connectDeleteConfirmButton'
import { deleteUserRoutine as routine } from 'modules/user/routines'
import saga from 'modules/user/sagas/deleteUserSaga'

function DeleteUserButton ({ onConfirm }) {
  return (
    <DeleteConfirmButton
      message="Are you sure want to delete this user?"
      onConfirm={onConfirm}
    />
  )
}

DeleteUserButton.propTypes = {
  onConfirm: PropTypes.func,
  user: ImmutablePropTypes.shape({ // eslint-disable-line react/no-unused-prop-types
    id: PropTypes.string.isRequired,
  }).isRequired,
}

export default connectDeleteConfirmButton({
  key: 'deleteUser',
  payloadProp: 'user',
  saga,
  routine,
})(DeleteUserButton)
