import React from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'

import Wrapper from './Wrapper'

function UserName ({ user }) {
  if (!user) {
    return null
  }

  if (user.has('firstName') || user.has('lastName')) {
    return (
      <Wrapper>
        {`${user.get('firstName', '')} ${user.get('lastName', '')} (${user.get('username')})`.trim()}
      </Wrapper>
    )
  }

  return (
    <Wrapper>
      {`${user.get('username')}`}
    </Wrapper>
  )
}

UserName.propTypes = {
  user: ImmutablePropTypes.shape({
    username: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }),
}

export default UserName
