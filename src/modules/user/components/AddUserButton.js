import React from 'react'
import AddButton from 'modules/list/components/AddButton'

function AddUserButton () {
  return (<AddButton to="/users/new" >Add user</AddButton>)
}

export default AddUserButton
