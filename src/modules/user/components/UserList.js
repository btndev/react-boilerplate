import React from 'react'
import ImmutablePropTypes from 'react-immutable-proptypes'
import withLoading from 'hoc/withLoading'
import List from 'modules/crud/components/List'
import ListHeader from 'modules/crud/components/ListHeader'

import UserListItem from './UserListItem'

function UserList ({ users }) {
  return (
    <List
      headers={[
        <ListHeader>First Name</ListHeader>,
        <ListHeader>Last Name</ListHeader>,
        <ListHeader>Username</ListHeader>,
      ]}
    >
      {
        users && users.map((user, index) => (
          <UserListItem key={user.get('id')} user={user} order={index + 1} />
        ))
      }
    </List>
  )
}

export default withLoading({ prop: 'users' })(UserList)

UserList.propTypes = {
  users: ImmutablePropTypes.list,
}
