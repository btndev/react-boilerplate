import React from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import ListItem from 'modules/crud/components/ListItem'
import ListCell from 'modules/crud/components/ListCell'
import EditUserButton from 'modules/user/components/EditUserButton'
import DeleteUserButton from 'modules/user/containers/DeleteUserButton'

function UserListItem ({ user, order }) {
  const actions = [
    <DeleteUserButton key="delete" user={user} />,
    <EditUserButton key="edit" user={user} />,
  ]

  return (
    <ListItem
      order={order}
      actions={actions}
    >
      <ListCell>
        {user.get('username')}
      </ListCell>
      <ListCell>
        {user.get('firstName')}
      </ListCell>
      <ListCell>
        {user.get('lastName')}
      </ListCell>
    </ListItem>
  )
}

UserListItem.propTypes = {
  order: PropTypes.number,
  user: ImmutablePropTypes.shape({
    id: PropTypes.string,
    username: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }).isRequired,
}

export default UserListItem
