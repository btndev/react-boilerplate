import React from 'react'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import EditButton from 'modules/list/components/EditButton'

function EditUserButton ({ user }) {
  return (<EditButton to={`/users/${user.get('id')}`} />)
}

EditUserButton.propTypes = {
  user: ImmutablePropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired,
}

export default EditUserButton
