/* eslint-disable import/prefer-default-export */
import { createRoutine } from 'modules/routine'
import { DELETE_USER } from './constans'

const deleteUserRoutine = createRoutine(DELETE_USER)

export {
  deleteUserRoutine,
}
