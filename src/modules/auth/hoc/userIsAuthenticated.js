import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect'
import { withRouter } from 'react-router'
import { routerActions } from 'react-router-redux'
import { selectIsAuthenticated, selectIsAuthenticating } from '../selectors'
import Authenticating from '../components/Authenticating'

const authWrapper = connectedRouterRedirect({
  redirectPath: '/login',
  authenticatedSelector: selectIsAuthenticated,
  authenticatingSelector: selectIsAuthenticating,
  wrapperDisplayName: 'UserIsAuthenticated',
  redirectAction: routerActions.replace,
  AuthenticatingComponent: Authenticating,
})

/**
 * Higher-Order Component that will restrict access only for authenticated user.
 * It will:
 *  * Wait for authentication process to finish.
 *  * Redirect unauthorized user to login page.
 *
 * @function
 * @name userIsAuthenticated
 * @param {Function} Component - Component to wrap.
 *
 * @returns {WrappedComponent} Wrapped component.
 */
const userIsAuthenticated =
  (Component) => withRouter(authWrapper(Component))

export default userIsAuthenticated
