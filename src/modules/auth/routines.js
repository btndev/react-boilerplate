/* eslint-disable import/prefer-default-export */
import { createRoutine } from 'modules/routine/utils'
import { AUTHENTICATE } from './constants'

export const authenticateRoutine = createRoutine(AUTHENTICATE)
