export const REDUCER_NAME = 'auth'
export const AUTH_TOKEN_NAME = 'authToken'

export const LOGOUT = 'LOGOUT'

export const AUTHENTICATE = 'AUTHENTICATE'

export const SET_AUTH_TOKEN = 'SET_AUTH_TOKEN'
export const CLEAR_AUTH_TOKEN = 'CLEAR_AUTH_TOKEN'
