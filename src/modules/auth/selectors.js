import { createSelector } from 'reselect'
import { REDUCER_NAME } from './constants'

const selectAuthState = (state) => (
  state.get(REDUCER_NAME)
)

export const selectUser = createSelector(
  selectAuthState,
  (auth) => auth.get('user')
)

export const selectUserId = createSelector(
  selectUser,
  (user) => user.get('id')
)

export const selectIsAuthenticating = createSelector(
  selectAuthState,
  (auth) => auth.get('isAuthenticating')
)

export const selectIsAuthenticated = createSelector(
  selectAuthState,
  (auth) => auth.get('isAuthenticated')
)
