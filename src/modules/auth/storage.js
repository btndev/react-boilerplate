import localStorage from 'services/localStorage'
import { AUTH_TOKEN_NAME } from './constants'

export const setAuthToken = (authToken) => {
  if (!authToken) {
    throw new Error('Empty authToken')
  }

  localStorage.setItem(AUTH_TOKEN_NAME, authToken)
}

export const getAuthToken = () => (
  localStorage.getItem(AUTH_TOKEN_NAME)
)

export const clearAuthToken = () => {
  localStorage.removeItem(AUTH_TOKEN_NAME)
}

export const hasAuthToken = () => {
  const authToken = getAuthToken()

  if (authToken === 'undefined') {
    return false
  }

  if (authToken === null) {
    return false
  }

  return !!authToken
}
