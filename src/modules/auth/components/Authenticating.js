import React from 'react'
import Loading from 'components/Loading'

function Authenticating () {
  return (<Loading>Authenticating...</Loading>)
}

export default Authenticating
