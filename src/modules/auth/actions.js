import { routineToPromiseAction } from 'modules/routine'

import {
  LOGOUT,
  SET_AUTH_TOKEN,
  CLEAR_AUTH_TOKEN,
} from './constants'
import { authenticateRoutine } from './routines'

export const authenticate = routineToPromiseAction(authenticateRoutine)

export function logout () {
  return {
    type: LOGOUT,
  }
}

export function setAuthToken (payload) {
  return {
    type: SET_AUTH_TOKEN,
    payload,
  }
}

export function clearAuthToken () {
  return {
    type: CLEAR_AUTH_TOKEN,
  }
}
