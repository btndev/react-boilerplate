import isString from 'lodash/isString'
import { parse } from 'utils/queryString'

const extractSearch = (searchOrLocation) => {
  if (isString(searchOrLocation)) {
    return searchOrLocation
  }

  if (Reflect.has(searchOrLocation, 'search') && isString(searchOrLocation.string)) {
    return searchOrLocation.search
  }

  throw new Error('Could not extract search query param')
}

function extractRedirectUrl (searchOrLocation) {
  const search = extractSearch(searchOrLocation)
  const { redirect } = parse(search)

  return redirect
}

export default extractRedirectUrl
