import { takeEvery, put, call, select } from 'redux-saga/effects'
import api from 'services/api'
import extractErrorMessage from 'modules/api/utils/extractErrorMessage'
import { selectLocation } from 'modules/router/selectors'
import { routerActions } from 'react-router-redux'
import extractRedirectUrl from '../utils/extractRedirectUrl'
import {
  authenticateRoutine,
} from '../routines'

function * redirectIfNeeded () {
  const { search } = yield select(selectLocation)
  const redirect = extractRedirectUrl(search)
  if (redirect) {
    yield put(routerActions.push(redirect))
  }
}

function * authenticate () {
  try {
    yield put(authenticateRoutine.request())
    const { data: { data } } = yield call(api.get, '/auth/me')
    yield put(authenticateRoutine.success(data))
    yield redirectIfNeeded()
  } catch (error) {
    yield put(authenticateRoutine.failure(extractErrorMessage(error)))
  } finally {
    yield put(authenticateRoutine.fulfill())
  }
}

function * authenticateSaga () {
  yield takeEvery(authenticateRoutine.TRIGGER, authenticate)
}

export default authenticateSaga
