import { take, put } from 'redux-saga/effects'
import {
  LOGOUT,
} from '../constants'
import { clearAuthToken } from '../actions'

function * logoutSaga () {
  while (true) {
    yield take(LOGOUT)
    yield put(clearAuthToken())
  }
}

export default logoutSaga
