import { take, fork, put, call, all } from 'redux-saga/effects'
import { setAuthToken, clearAuthToken } from '../storage'
import { authenticate } from '../actions'
import {
  SET_AUTH_TOKEN,
  CLEAR_AUTH_TOKEN,
} from '../constants'

function * setTokenSaga () {
  while (true) {
    const action = yield take(SET_AUTH_TOKEN)
    const { payload: authToken } = action
    yield call(setAuthToken, authToken)
    yield put(authenticate(authToken))
  }
}

function * clearTokenSaga () {
  while (true) {
    yield take(CLEAR_AUTH_TOKEN)
    yield call(clearAuthToken)
  }
}

function * tokenSaga () {
  yield all([
    fork(setTokenSaga),
    fork(clearTokenSaga),
  ])
}

export default tokenSaga
