import tokenSaga from './tokenSaga'
import logoutSaga from './logoutSaga'
import authenticateSaga from './authenticateSaga'

const sagas = [
  tokenSaga,
  logoutSaga,
  authenticateSaga,
]

export default sagas
