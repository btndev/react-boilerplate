import { Component } from 'react'
import PropTypes from 'prop-types'
import { authenticate } from 'modules/auth/actions'
import { hasAuthToken, getAuthToken, clearAuthToken } from 'modules/auth/storage'
import { connect } from 'react-redux'

import { mapStateToProps } from './selectors'

/*
 * Action trigger component for authenticating user user.
 * If there is token in token storage it will try to authenticate user with it.
 */
class AuthenticateUser extends Component {
  componentWillMount () {
    this.loginWithAuthToken()
  }

  shouldComponentUpdate () {
    return false
  }

  loginWithAuthToken () {
    const { isAuthenticated } = this.props

    if (isAuthenticated) {
      return
    }

    if (!hasAuthToken()) {
      return
    }

    const { dispatch } = this.props

    dispatch(authenticate(getAuthToken())).catch(() => {
      clearAuthToken()
    })
  }

  render () {
    return null
  }
}

AuthenticateUser.propTypes = {
  isAuthenticated: PropTypes.bool,
  dispatch: PropTypes.func,
}

export default connect(mapStateToProps)(AuthenticateUser)
