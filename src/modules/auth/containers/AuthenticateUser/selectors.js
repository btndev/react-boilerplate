/* eslint-disable import/prefer-default-export */
import { createStructuredSelector } from 'reselect'
import {
  selectIsAuthenticated,
  selectIsAuthenticating,
} from 'modules/auth/selectors'

export const mapStateToProps = createStructuredSelector({
  isAuthenticated: selectIsAuthenticated,
  isAuthenticating: selectIsAuthenticating,
})
