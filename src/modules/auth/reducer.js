import { fromJS, Map as iMap } from 'immutable'
import {
  LOGOUT,
  SET_AUTH_TOKEN,
  CLEAR_AUTH_TOKEN,
} from './constants'

import { authenticateRoutine } from './routines'

const initialState = iMap({
  error: iMap(),
  isAuthenticated: null,
  isAuthenticating: null,
  authToken: null,
  token: iMap({
    id: null,
  }),
})

function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case authenticateRoutine.TRIGGER:
    case authenticateRoutine.REQUEST:
      return state.merge({
        error: iMap(),
        isAuthenticating: true,
      })
    case authenticateRoutine.SUCCESS:
      return state.merge({
        error: iMap(),
        isAuthenticating: false,
        isAuthenticated: true,
        user: fromJS(action.payload),
      })
    case authenticateRoutine.ERROR:
      return state.merge({
        error: fromJS(action.error) || iMap(),
        isAuthenticating: false,
        isAuthenticated: false,
        authToken: null,
        user: iMap(),
      })
    case SET_AUTH_TOKEN:
      return state.merge({
        authToken: action.payload,
      })
    case CLEAR_AUTH_TOKEN:
      return state.merge({
        isAuthenticating: false,
        isAuthenticated: false,
        authToken: null,
      })
    case LOGOUT:
      return initialState
    default:
      return state
  }
}

export default reducer
