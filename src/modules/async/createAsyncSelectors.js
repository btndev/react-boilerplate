import { createSelector, createStructuredSelector } from 'reselect'

/**
 * Async {@link https://github.com/reactjs/reselect|reselect} selectors functions, Redux `mapStateToProps` function
 *
 * @typedef {Object} AsyncSelectors
 * @property {Function} selectState - Whole state selector
 * @property {Function} selectLoading - Select if async action flow is in progress
 * @property {Function} selectData - Select data from async flow
 * @property {Function} selectError - Select error from async flow
 * @property {Function} selectSuccess - Select if async flow succeeded
 * @property {Function} mapStateToProps - structured selector from all selectors
 */

/**
 * Create selectors for {@link asyncReducer}.
 *
 * @see {@link https://github.com/reactjs/reselect}
 *
 * @param {Function} selectState - Select state function that will be starting point for selectors.
 * @returns {AsyncSelectors} - Selector functions for specific state.
 */
function createAsyncSelectors (selectState) {
  const selectLoading = createSelector(
    selectState,
    (state) => (state.get('loading'))
  )

  const selectError = createSelector(
    selectState,
    (state) => (state.get('error'))
  )

  const selectData = createSelector(
    selectState,
    (state) => (state.get('data'))
  )

  const selectSuccess = createSelector(
    selectState,
    (state) => (state.get('success'))
  )

  const mapStateToProps = createStructuredSelector({
    loading: selectLoading,
    error: selectError,
    data: selectData,
    success: selectSuccess,
  })

  return {
    selectState,
    selectLoading,
    selectData,
    selectError,
    selectSuccess,
    mapStateToProps,
  }
}

export default createAsyncSelectors
