import { fromJS } from 'immutable'

const haveAction = (types, type) => {
  if (!types) {
    return false
  }

  return types.indexOf(type) >= 0
}

/**
 * Create reducer for async flow that will handle specific actions with specific changes.
 *
 * @param {Object} options - Options.
 * @param {?Array<String>} options.initActions - Actions that will return initial state.
 * @param {?Array<String>} options.requestActions - Actions that will modify state with loading phase.
 * @param {?Array<String>} options.successActions - Actions that will modify state with success phase.
 * @param {?Array<String>} options.errorActions - Actions that will modify state with error phase.
 *
 * @returns {asyncReducer} - Reducer function.
 * @returns {typeof asyncReducer} - IDE type check.
 */
function createAsyncReducer ({
  initActions,
  requestActions,
  successActions,
  errorActions,
}) {
  const initialState = fromJS({
    data: null,
    success: false,
    loading: false,
    error: null,
  })

  /**
   * @function
   * @name asyncReducer
   *
   * @param {*} state - State object.
   * @param {Object} action - Action.
   *
   * @returns {*} - State.
   */
  return function asyncReducer (state = initialState, action) {
    const { type } = action

    if (haveAction(initActions, type)) {
      return initialState
    }

    if (haveAction(requestActions, type)) {
      return state.merge({
        success: false,
        loading: true,
      })
    }

    if (haveAction(successActions, type)) {
      return state.merge({
        data: action.payload,
        success: true,
        loading: false,
      })
    }

    if (haveAction(errorActions, type)) {
      return state.merge({
        error: action.payload,
        success: false,
        loading: false,
      })
    }

    return state
  }
}

export default createAsyncReducer
