import { DESTROY_PAGE, INIT_PAGE } from './constants'

/**
 * Initialize page action.
 *
 * @param {PageKey} pageKey - Page key.
 * @returns {Object} - Redux action object.
 */
function initPage (pageKey) {
  return {
    type: INIT_PAGE,
    pageKey,
  }
}

/**
 * Destroy page action.
 *
 * @param {PageKey} pageKey - Page key.
 * @returns {Object} - Redux action object.
 */
function destroyPage (pageKey) {
  return {
    type: DESTROY_PAGE,
    pageKey,
  }
}

export {
  initPage,
  destroyPage,
}
