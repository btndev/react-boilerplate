import compose from 'recompose/compose'
import { withRouter as withRouterHoc } from 'react-router'
import { connect } from 'react-redux'
import invariant from 'invariant'
import warning from 'warning'
import withReducer from 'hoc/withReducer'
import withSaga from 'hoc/withSaga'
import pageContainer from './pageContainer'
import decoratePageReducer from '../utils/decoratePageReducer'

/**
 * Composition function for pages. It's a combination of most common HOC used with pages like: {@link pageContainer}, {@link withReducer}, {@link withSaga},
 * {@link https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/api/withRouter.md|withRouter} ({@link https://github.com/ReactTraining/react-router|react-router}),
 * {@link https://github.com/reactjs/react-redux/blob/master/docs/api.md#connectmapstatetoprops-mapdispatchtoprops-mergeprops-options|connect} ({@link https://github.com/reactjs/react-redux|react-redux}).
 * Reducer will be also decorated using {@link decoratePageReducer}.
 *
 * @param {Object} options - Options.
 * @param {PageKey} options.key - Unique page key that will be used to inject reducer, and start saga.
 * @param {?Function} options.reducer - Reducer function that will be automatically injected to rest of reducers using {@link withReducer}.
 * @param {?Generator} options.saga - Saga function that will be automatically stated on entering page, and stopped on leaving page using {@link withSaga}.
 * @param {?Boolean} options.withRouter - Component will automatically be wrapped by react-router {@link withRouter} HOC.
 * @param {?Function} options.mapStateToProps - Function for Redux compose.
 * @param {?Function} options.mapDispatchToProps - Function for Redux compose.
 *
 * @returns {ComponentWrapperFunction} - Component wrapper function.
 */
function connectPage ({
  key,
  reducer,
  saga,
  mapStateToProps,
  withRouter,
  mapDispatchToProps,
}) {
  if (process.env.NODE_ENV !== 'production' && (saga || reducer)) {
    invariant(!!key, 'Key is required')
    warning(key.match(/^.+Page/), `Page key should have "Page" sufix, current key: ${key}`)
  }

  const composeArgs = [
    key && pageContainer(key),
    reducer && withReducer({ key, reducer: decoratePageReducer(key)(reducer) }),
    saga && withSaga({ key, saga }),
    withRouter && withRouterHoc,
    (mapStateToProps || mapDispatchToProps) && connect(mapStateToProps, mapDispatchToProps),
  ].filter(Boolean)

  return (WrappedComponent) => compose(...composeArgs)(WrappedComponent)
}

export default connectPage
