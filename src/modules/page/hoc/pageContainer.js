import React, { Component } from 'react'
import PropTypes from 'prop-types'
import hoistNonReactStatics from 'hoist-non-react-statics'
import wrapDisplayName from 'recompose/wrapDisplayName'
import { initPage, destroyPage } from '../actions'

/**
 * Higher-Order Components that wraps Page. It will dispatch {@link initPage} action on component mount, {@link destroyPage} action on component unmount mainly for {@link decoratePageReducer}.
 *
 * @param {PageKey} pageKey - Page key.
 */
function pageContainer (pageKey) {
  return (WrappedComponent) => {
    class PageContainer extends Component {
      componentWillMount () {
        const { store } = this.context

        store.dispatch(initPage(pageKey))
      }

      componentWillUnmount () {
        const { store } = this.context

        store.dispatch(destroyPage(pageKey))
      }

      render () {
        return (<WrappedComponent {...this.props} />)
      }
    }

    PageContainer.displayName = wrapDisplayName(WrappedComponent, 'pageContainer')

    PageContainer.contextTypes = {
      store: PropTypes.object.isRequired,
    }

    return hoistNonReactStatics(PageContainer, WrappedComponent)
  }
}

export default pageContainer
