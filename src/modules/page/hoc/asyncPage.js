import React from 'react'
import asyncComponent from 'hoc/asyncComponent'

/**
 * Higher-Order Component that will lazy load page component using {@link https://github.com/thejameskyle/react-loadable|react-loadable} to utilize webpack {@link https://webpack.js.org/guides/code-splitting/|code splitting}.
 *
 * @see {@link https://webpack.js.org/guides/code-splitting/}
 *
 * @param {Object} options - Available options: {@link https://github.com/thejameskyle/react-loadable|react-loadable}.
 *
 * @returns {WrappedComponent} Wrapped component.
 */
function asyncPage (options) {
  return asyncComponent({
    ...options,
    render (loaded, props) {
      const Component = loaded.default || loaded

      return (<Component {...props} />)
    },
  })
}

export default asyncPage
