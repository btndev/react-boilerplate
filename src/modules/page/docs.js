/**
 * Page specific unique identifier
 *
 * @typedef {String} PageKey
 */
