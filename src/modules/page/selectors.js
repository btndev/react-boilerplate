/* eslint-disable import/prefer-default-export */
import { createSelector } from 'reselect'

const makeSelectSubState = (subState) => (state) => (state.get(subState))

/**
 * Select specific page main state from global Redux state.
 *
 * @see {@link https://github.com/reactjs/reselect}
 *
 * @param {PageKey} pageKey - Page key.
 * @param {?string} subState - Subset of page state.
 *
 * @returns {Function} - Selector function.
 */
export function makeSelectPageState (pageKey, subState) {
  const selectPageState = (state) => (state.get(pageKey))

  if (subState) {
    return createSelector(selectPageState, makeSelectSubState(subState))
  }

  return selectPageState
}
