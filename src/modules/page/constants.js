/**
 * Initialize page action type
 * @type {String}
 */
export const INIT_PAGE = 'page/INIT_PAGE'

/**
 * Destroy page action type
 * @type {String}
 */
export const DESTROY_PAGE = 'page/DESTROY_PAGE'
