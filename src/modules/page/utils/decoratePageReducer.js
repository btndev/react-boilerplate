import {
  INIT_PAGE,
  DESTROY_PAGE,
} from '../constants'

/**
 * Decorate reducer with reducer that will automatically reset page state in Redux global state when page component is mount/unmount.
 * {@link initPage}, {@link destroyPage} actions are dispatched by {@link pageContainer} HOC usually added using {@link connectPage} HOC.
 *
 * @param {PageKey} pageKey - Page key.
 */
function decoratePageReducer (pageKey) {
  return (reducer) => {
    const decoratedPageReducer = (state, action) => {
      if (action.type === INIT_PAGE && action.pageKey === pageKey) {
        return reducer(undefined, action)
      }

      if (action.type === DESTROY_PAGE && action.pageKey === pageKey) {
        return reducer(undefined, action)
      }

      return reducer(state, action)
    }

    if (process.env.NODE_ENV === 'development') {
      const { default: wrapDisplayName } = require('recompose/wrapDisplayName') // eslint-disable-line global-require
      decoratedPageReducer.displayName = wrapDisplayName(reducer, `decoratedPageReducer(${pageKey})`)
    }

    return decoratedPageReducer
  }
}

export default decoratePageReducer
