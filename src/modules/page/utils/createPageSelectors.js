import { createRoutineSelectors } from 'modules/routine'
import { makeSelectPageState } from '../selectors'

/**
 * @typedef {Object} PageSelectors
 * @property {Function} selectState
 */

/**
 * Create selector functions for specific page (page specific Redux state from global state).
 *
 * @see {@link https://github.com/reactjs/reselect}
 *
 * @param {PageKey} pageKey - Page key.
 * @param {?string} subState - Sub state from {@link combineReducers}/{@link createStructuredRoutineReducer}.
 *
 * @returns {PageSelectors} - Page selectors for specific page.
 */
function createPageSelectors (pageKey, subState = undefined) {
  return createRoutineSelectors(makeSelectPageState(pageKey, subState))
}

export default createPageSelectors
