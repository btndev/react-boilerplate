/**
 * Routine Action Creator.
 *
 * @typedef {Function} RoutineActionCreator
 * @type Function
 * @param {?*} payload
 *
 * @returns {Object} Redux action
 */

/**
 * Redux saga routine from {@link https://github.com/afitiskin/redux-saga-routines|afitiskin/redux-saga-routines}
 *
 * @typedef {Object} Routine
 * @property {String} TRIGGER - Trigger action type
 * @property {String} REQUEST - Request action type
 * @property {String} SUCCESS - Success action type
 * @property {String} FAILURE - Failure action type
 * @property {String} FULFILL - Fulfill action type
 *
 * @property {RoutineActionCreator} trigger - Trigger action creator
 * @property {RoutineActionCreator} request - Request action creator
 * @property {RoutineActionCreator} success - Success action creator
 * @property {RoutineActionCreator} failure - Failure action creator
 * @property {RoutineActionCreator} fulfill - Fulfill action creator
 *
 * @see {@link https://github.com/afitiskin/redux-saga-routines}
 */
