import { createAsyncReducer } from 'modules/async'

/**
 * Create reducer function that will handle specific routine.
 *
 * @param {Routine} routine - Routine that will control reducer.
 * @returns {typeof asyncReducer} - Reducer function for routine.
 */
function createRoutineReducer (routine) {
  /**
   * Routine reducer created with {@link asyncReducer}.
   *
   * @function
   * @name routineReducer
   *
   * @returns {asyncReducer} - Async reducer created from {@link Routine}.
   */
  const routineReducer = createAsyncReducer({
    requestActions: [routine.REQUEST],
    successActions: [routine.SUCCESS],
    errorActions: [routine.FAILURE],
  })

  if (process.env.NODE_ENV === 'development') {
    const { default: getRoutinePrefix } = require('./getRoutinePrefix') // eslint-disable-line global-require
    routineReducer.displayName = `routineReducer(${getRoutinePrefix(routine)})`
  }

  return routineReducer
}

export default createRoutineReducer
