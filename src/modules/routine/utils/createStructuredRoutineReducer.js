import { combineReducers } from 'redux-immutable'
import createRoutineReducer from './createRoutineReducer'
import isRoutine from './isRoutine'

/**
 * Create structured, combined reducer from multiple rutines/reducers.
 *
 * @param {Object} routines - Routines object map where key is Redux state key and value is routine that should be converted to reducer with {@link createRoutineReducer} or regular reducer function.
 *
 * @returns {Function} - Combined reducer funcion created from map of {@link Routine} using {@link createRoutineReducer}.
 */
function createStructuredRoutineReducer (routines) {
  const objectKeys = Object.keys(routines)
  const reducersMap = objectKeys.reduce((acc, key) => ({
    ...acc,
    [key]: isRoutine(routines[key]) ? createRoutineReducer(routines[key]) : routines[key],
  }), {})

  return combineReducers(reducersMap)
}

export default createStructuredRoutineReducer
