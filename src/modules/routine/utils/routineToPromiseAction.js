import { ROUTINE_PROMISE_ACTION } from 'redux-saga-routines'

/**
 * Redux Thunk Promise Action.
 *
 * @see {@link https://github.com/gaearon/redux-thunk|redux-thunk}
 *
 * @typedef {Function} ThunkPromiseAction
 * @param {Function} dispatch - Redux dispatch function.
 *
 * @returns {Promise} - Promise.
 */

/**
 * Convert routine to promise action using {@link https://github.com/gaearon/redux-thunk|redux-thunk}.
 *
 * @see {@link https://github.com/gaearon/redux-thunk}
 *
 * @param {Routine} routine - Routine.
 *
 * @returns {routinePromiseAction} - Action creator that will return `redux-thunk` action that will converted to {@link Promise} controlled by {@link Routine} flow.
 * @returns {typeof routinePromiseAction} - IDE type check.
 */
function routineToPromiseAction (routine) {
  /**
   * Routine Promise Action.
   *
   * @function
   * @name routinePromiseAction
   * @param {*} values - Payload for {@link Routine} {@link Routine.trigger|trigger} action.
   *
   * @returns {ThunkPromiseAction} - Redux Thunk Promise Action.
   */
  return (values) => (dispatch) => new Promise((resolve, reject) => dispatch({
    type: ROUTINE_PROMISE_ACTION,
    payload: {
      values,
      routine,
      defer: { resolve, reject },
    },
  }))
}

export default routineToPromiseAction
