import { bindRoutineToReduxForm } from 'redux-saga-routines'

/**
 * Convert routine to Redux form {@link https://redux-form.com/7.1.1/docs/api/reduxform.md/#-code-onsubmit-function-code-optional-|onSubmit} action.
 *
 * @param {Routine} routine - Redux routine.
 * @returns {reduxFormOnSubmitAction} - Function with `redux-form` {@link https://redux-form.com/7.1.1/docs/api/reduxform.md/#-code-onsubmit-function-code-optional-|onSubmit} signature.
 * @returns {typeof reduxFormOnSubmitAction} - IDE type check.
 */
function routineToReduxFormAction (routine) {
  const boundReduxFormAction = bindRoutineToReduxForm(routine)

  /**
   * @name reduxFormOnSubmitAction
   *
   * @param {*} payload - Action payload.
   * @param {Function} dispatch - Redux dispatch function.
   * @param {Object} props - Redux form props.
   *
   * @returns {ThunkPromiseAction} - Redux Thunk Promise Action.
   */
  return (payload, dispatch, props) => { // eslint-disable-line arrow-body-style
    // return redux-thunk action wrapper
    return (dispatch) => (boundReduxFormAction(payload, dispatch, props)) // eslint-disable-line no-shadow
  }
}

export default routineToReduxFormAction
