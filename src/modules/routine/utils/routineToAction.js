/**
 * Extract trigger action creator function from routine.
 *
 * @param {Routine} routine - Routine.
 * @returns {RoutineActionCreator} - Redux action creator.
 */
function routineToAction (routine) {
  return routine.trigger
}

export default routineToAction
