
const SEPARATOR = '/'

function getRoutinePrefix (routine) {
  return `${routine}`.substring(0, `${routine}`.indexOf(SEPARATOR))
}

export default getRoutinePrefix
