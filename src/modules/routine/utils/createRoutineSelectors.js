import { createAsyncSelectors as createRoutineSelectors } from 'modules/async'

/**
 * Alias for {@link createAsyncSelectors}.
 *
 * @see {@link createAsyncSelectors}
 *
 * @function
 * @name createRoutineSelectors
 */
export default createRoutineSelectors
