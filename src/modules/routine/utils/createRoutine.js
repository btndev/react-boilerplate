import { createRoutine as baseCreateRoutine } from 'redux-saga-routines'

function defaultPayloadCreator (payload) {
  return payload
}

function defaultMetaCreator (payload, meta) {
  return meta
}

/**
 * Create {@link https://github.com/afitiskin/redux-saga-routines|redux-saga-routines} routine.
 *
 * @see {@link https://github.com/afitiskin/redux-saga-routines}
 *
 * @param {string} prefix - Routine prefix.
 * @param {?Function} payloadCreator - Payload creator function.
 * @param {?Function} metaCreator - Meta creator function.
 * @returns {Routine} - Routine.
 */
function createRoutine (prefix, payloadCreator = defaultPayloadCreator, metaCreator = defaultMetaCreator) {
  return baseCreateRoutine(prefix, payloadCreator, metaCreator)
}

export default createRoutine
