
function isRoutine (routine) {
  return routine && routine.REQUEST && routine.SUCCESS && routine.FAILURE
}

export default isRoutine
