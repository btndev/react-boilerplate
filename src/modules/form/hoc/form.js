import compose from 'recompose/compose'
import { reduxForm } from 'redux-form/immutable'
import withFormLoading from './withFormLoading'
import createFormValidator from '../utils/createFormValidator'
/**
 * Composition function for form components. wrapper for {@link https://redux-form.com/7.1.1/docs/api/reduxform.md/|reduxForm} with some extra features.
 *
 * @see {@link https://redux-form.com/7.1.1/docs/api/reduxform.md/} for reduxForm options
 *
 * @param {Object} options - {@link https://redux-form.com/7.1.1/docs/api/reduxform.md/|Rest of options}.
 * @param {?Boolean} options.withLoading - When true, form will be composed with {@link withFormLoading} HOC.
 * @param {?AjvSchema} options.schema - Validation schema for {@link createFormValidator}.
 *
 * @returns {ComponentWrapperFunction} - Component wrapper function.
 */
function form ({ withLoading, schema, ...restOptions }) {
  const reduxFormConfig = {
    persistentSubmitErrors: false,
    ...restOptions,
  }

  if (schema) {
    reduxFormConfig.validate = createFormValidator(schema)
  }

  const composeArgs = [
    withLoading && withFormLoading(),
    reduxForm(reduxFormConfig),
  ].filter(Boolean)

  return (WrappedComponent) => compose(...composeArgs)(WrappedComponent)
}

export default form
