import withLoading from 'hoc/withLoading'

function withFormLoading () {
  return withLoading({ prop: 'initialValues' })
}

export default withFormLoading
