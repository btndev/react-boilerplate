import { actionTypes, reset, hasSubmitSucceeded } from 'redux-form/immutable'

const { CHANGE } = actionTypes

function handleChangeAction (action, store) {
  const { meta: { form } } = action
  const submitSucceeded = hasSubmitSucceeded(form)(store.getState())

  if (!submitSucceeded) {
    return
  }

  store.dispatch(reset(form))
}

/**
 * Middleware for reset redux form {@link https://redux-form.com/7.1.1/docs/api/props.md/#-code-submitsucceeded-boolean-code-|submitSucceeded} prop automatically on first change of any input field.
 *
 * @param {ReduxStore} store - Redux store.
 */
function resetFormSubmitSucceededMiddleware (store) {
  return (next) => (action) => {
    if (action.type === CHANGE) {
      handleChangeAction(action, store)
    }

    return next(action)
  }
}

export default resetFormSubmitSucceededMiddleware
