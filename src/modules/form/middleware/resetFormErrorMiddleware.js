import { actionTypes, stopSubmit, getFormError } from 'redux-form/immutable'

const { CHANGE } = actionTypes

function handleChangeAction (action, store) {
  const { meta: { form } } = action
  const submitError = getFormError(form)(store.getState())

  if (!submitError) {
    return
  }

  store.dispatch(stopSubmit(form, {}))
}

/**
 * Middleware for reset submission errors automatically on first change of any input field.
 *
 * @see {@link https://github.com/erikras/redux-form/issues/2604#issuecomment-282670843}
 *
 * @param {ReduxStore} store - Redux store.
 */
function resetFormErrorMiddleware (store) {
  return (next) => (action) => {
    if (action.type === CHANGE) {
      handleChangeAction(action, store)
    }

    return next(action)
  }
}

export default resetFormErrorMiddleware
