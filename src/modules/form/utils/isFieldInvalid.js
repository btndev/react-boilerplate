import isFieldValid from './isFieldValid'

function isFieldInvalid (props) {
  const isValid = isFieldValid(props)

  return isValid === false ? true : undefined
}

export default isFieldInvalid
