import isFieldValid from './isFieldValid'

describe('isFieldValid', () => {
  function generateProps (meta) {
    return {
      meta: {
        touched: false,
        active: false,
        valid: true,
        ...meta,
      },
    }
  }

  it('should return undefined for untouched field', () => {
    const props = generateProps({
      touched: false,
    })

    expect(isFieldValid(props)).toBe(undefined)
  })

  it('should return undefined for active field', () => {
    const props = generateProps({
      active: true,
    })

    expect(isFieldValid(props)).toBe(undefined)
  })

  it('should return true for valid and touched field', () => {
    const props = generateProps({
      touched: true,
      valid: true,
    })

    expect(isFieldValid(props)).toBe(true)
  })

  it('should return false for invalid and touched field', () => {
    const props = generateProps({
      touched: true,
      valid: false,
    })

    expect(isFieldValid(props)).toBe(false)
  })
})
