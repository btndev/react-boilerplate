
function isMetaValid (meta) {
  if (meta.active) {
    return undefined
  }

  if (!meta.touched) {
    return undefined
  }

  return meta.valid
}

function isFieldValid (props) {
  if (props.meta) {
    return isMetaValid(props.meta)
  }

  throw new Error('Missing redux-form meta prop')
}

export default isFieldValid
