import { SubmissionError } from 'redux-form/immutable'
import extractErrorMessage from 'modules/api/utils/extractErrorMessage'

/**
 * Convert Error to `redux-form` `SubmissionError`.
 *
 * @see {@link https://redux-form.com/7.1.1/docs/api/submissionerror.md/}
 *
 * @param {object|string} error - Error object.
 * @returns {SubmissionError} - Redux form submission exception.
 */
function createFormSubmissionError (error) {
  const submissionError = {
    _error: extractErrorMessage(error),
  }

  if (process.env.NODE_ENV === 'development') {
    console.error('submissionError@createFormSubmissionError', submissionError) // eslint-disable-line no-console
  }

  return new SubmissionError(submissionError)
}

export default createFormSubmissionError
