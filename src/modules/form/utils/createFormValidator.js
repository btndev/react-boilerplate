import validate from 'redux-form-with-ajv'
import errorMessage from 'modules/validation/errorMessage'

/**
 * Create Redux form validator from AjvSchema.
 *
 * @param {AjvSchema} schema - Ajv schema.
 *
 * @returns {Function} - Redux form {@link https://redux-form.com/7.1.1/docs/api/reduxform.md/#-code-validate-values-object-props-object-gt-errors-object-code-optional-|validator}.
 */
function createFormValidator (schema) {
  return validate(schema, { errorMessage })
}

export default createFormValidator
