import React from 'react'
import PropTypes from 'prop-types'
import { Label as BaseLabel } from 'reactstrap'

function Label ({
  name,
  htmlFor,
  label,
  children,
  input,
  meta,
  ...restProps
}) {
  return (
    <BaseLabel for={name || htmlFor} {...restProps}>
      {label || children}
    </BaseLabel>
  )
}

Label.propTypes = {
  ...BaseLabel.propTypes,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  name: PropTypes.string,
  htmlFor: PropTypes.string,
  children: PropTypes.node,

}

export default Label
