import React from 'react'
import PropTypes from 'prop-types'
import Field from 'modules/form/components/Field'
import FormRow from 'modules/form/components/FormRow'

function FormRowField (props) {
  return (
    <Field component={FormRow} {...props} />
  )
}

FormRowField.propTypes = {
  ...Field.propTypes,
  ...FormRowField.propTypes,
  component: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
}

export default FormRowField
