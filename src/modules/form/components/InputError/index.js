import React from 'react'
import PropTypes from 'prop-types'
import { FormFeedback } from 'reactstrap'

function InputError (props) {
  const { meta: { touched, error } } = props

  if (!touched) {
    return null
  }

  return (
    <FormFeedback>
      {error}
    </FormFeedback>
  )
}

InputError.propTypes = {
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }).isRequired,
}

export default InputError
