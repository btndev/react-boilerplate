import React from 'react'
import PropTypes from 'prop-types'
import FormGroup from 'modules/form/components/FormGroup'
import Label from 'modules/form/components/Label'
import Input from 'modules/form/components/Input'
import Column from 'modules/layout/components/Column'

function FormRow ({ labelSize, ...restProps }) {
  if (labelSize) {
    return (
      <FormGroup row>
        <Label {...restProps} sm={labelSize} />
        <Column sm={12 - labelSize}>
          <Input {...restProps} />
        </Column>
      </FormGroup>
    )
  }

  return (
    <FormGroup>
      <Label {...restProps} />
      <Input {...restProps} />
    </FormGroup>
  )
}

FormRow.propTypes = {
  ...Label.propTypes,
  ...Input.propTypes,
  labelSize: PropTypes.number,
}

export default FormRow
