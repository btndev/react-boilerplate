import React from 'react'
import PropTypes from 'prop-types'
import Success from 'components/Success'

function SuccessMessage (props) {
  const { submitSucceeded, submitting, children } = props

  if (submitting) {
    return null
  }

  if (!submitSucceeded) {
    return null
  }

  return (
    <Success>
      { children || 'Changes saved' }
    </Success>
  )
}

SuccessMessage.propTypes = {
  submitting: PropTypes.bool,
  submitSucceeded: PropTypes.bool,
  children: PropTypes.node,
}

export default SuccessMessage
