import React from 'react'
import { Input as BaseInput } from 'reactstrap'
import PropTypes from 'prop-types'
import InputError from 'modules/form/components/InputError'

import isFieldValid from 'modules/form/utils/isFieldValid'
import isFieldInvalid from 'modules/form/utils/isFieldInvalid'

function Input (props) {
  const { input, type, placeholder } = props

  const valid = isFieldValid(props)
  const invalid = isFieldInvalid(props)

  return ([
    <BaseInput key="input" {...input} valid={valid} invalid={invalid} placeholder={placeholder} type={type} />,
    <InputError key="error" {...props} />,
  ])
}

Input.propTypes = {
  inline: PropTypes.bool,
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
  }),
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
}

export default Input
