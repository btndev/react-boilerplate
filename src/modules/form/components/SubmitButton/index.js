import React from 'react'
import PropTypes from 'prop-types'
import Button from 'components/Button'

function SubmitButton (props) {
  const { children, submitting, invalid, dirty } = props

  const disabled = submitting || invalid || !dirty

  return (
    <Button type="submit" color="success" disabled={disabled} >
      { children }
    </Button>
  )
}

SubmitButton.propTypes = {
  invalid: PropTypes.bool,
  dirty: PropTypes.bool,
  submitting: PropTypes.bool,
  children: PropTypes.node,
}

SubmitButton.defaultProps = {
  children: 'Submit',
}

export default SubmitButton
