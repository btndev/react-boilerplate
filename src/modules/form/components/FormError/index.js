import React from 'react'
import PropTypes from 'prop-types'
import Error from 'components/Error'

import Wrapper from './Wrapper'

function FormError ({ error }) {
  if (!error) {
    return null
  }

  return (
    <Wrapper>
      <Error alert>{error}</Error>
    </Wrapper>
  )
}

FormError.propTypes = {
  error: PropTypes.string,
}

export default FormError
