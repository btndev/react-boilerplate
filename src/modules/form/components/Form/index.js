import React from 'react'
import PropTypes from 'prop-types'
import { Form as BaseForm } from 'reactstrap'

import FormError from 'modules/form/components/FormError'
import SuccessMessage from 'modules/form/components/SuccessMessage'
import SubmitButton from 'modules/form/components/SubmitButton'

function Form ({
  children,
  onSubmit,
  submitLabel,
  successMessage,
  ...restProps
}) {
  return (
    <BaseForm onSubmit={onSubmit}>
      <FormError {...restProps} />
      {successMessage && <SuccessMessage {...restProps} >{successMessage}</SuccessMessage>}
      {children}
      {submitLabel && <SubmitButton {...restProps}>{submitLabel}</SubmitButton>}
    </BaseForm>
  )
}

Form.propTypes = {
  ...FormError.propTypes,
  onSubmit: PropTypes.func.isRequired,
  successMessage: PropTypes.string,
  submitLabel: PropTypes.string,
}

export default Form
