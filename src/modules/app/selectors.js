import { createSelector } from 'reselect'

const selectAppState = (state) => (
  state.get('app')
)

const selectIsNavbarOpen = createSelector(
  selectAppState,
  (layoutState) => (layoutState.get('isNavbarOpen'))
)

const selectIsDropdownOpen = createSelector(
  selectAppState,
  (layoutState) => (layoutState.get('isDropdownOpen'))
)

export {
  selectAppState,
  selectIsNavbarOpen,
  selectIsDropdownOpen,
}
