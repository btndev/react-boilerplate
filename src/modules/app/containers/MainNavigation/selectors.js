/* eslint-disable import/prefer-default-export */
import { createStructuredSelector } from 'reselect'
import { selectIsAuthenticated } from 'modules/auth/selectors'

const mapStateToProps = createStructuredSelector({
  isAuthenticated: selectIsAuthenticated,
})

export {
  mapStateToProps,
}
