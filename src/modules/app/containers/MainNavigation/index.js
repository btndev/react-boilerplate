import React from 'react'
import PropTypes from 'prop-types'
import {
  Nav,
  NavItem,
} from 'reactstrap'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import Icon from 'components/Icon'

import NavLink from './NavLink'
import { mapStateToProps } from './selectors'

function MainNavigation ({ isAuthenticated }) {
  return (
    <Nav navbar>
      <NavItem>
        <NavLink to="/" exact>
          <Icon name="home" />
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink to="/counter" exact >Counter</NavLink>
      </NavItem>
      {
        isAuthenticated &&
        <NavItem>
          <NavLink to="/users">Users</NavLink>
        </NavItem>
      }
    </Nav>
  )
}

MainNavigation.propTypes = {
  isAuthenticated: PropTypes.bool,
}

export default withRouter(connect(mapStateToProps)(MainNavigation))
