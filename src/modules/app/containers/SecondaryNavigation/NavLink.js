import React from 'react'
import { NavLink as ReactRouterNavLink } from 'react-router-dom'
import { NavLink as BootstrapNavLink } from 'reactstrap'

function NavLink (props) {
  return (<BootstrapNavLink tag={ReactRouterNavLink} {...props} />)
}

NavLink.propTypes = {
  ...BootstrapNavLink.propTypes,
  ...ReactRouterNavLink.propTypes,
}

export default NavLink
