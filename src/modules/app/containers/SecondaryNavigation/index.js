import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import {
  Nav,
  NavItem,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
} from 'reactstrap'
import { openDropdown, closeDropdown } from 'modules/app/action'
import Icon from 'components/Icon'

import DropdownItemLink from './DropdownItemLink'
import { mapStateToProps } from './selectors'

import NavLink from './NavLink'

class SecondaryNavigation extends PureComponent {
  handleToggleDropdown = () => {
    const { IsDropdownOpen, handleOpenDropdown, handleCloseDropdown } = this.props

    IsDropdownOpen ? handleCloseDropdown() : handleOpenDropdown()
  }

  render () {
    const { isAuthenticated, IsDropdownOpen } = this.props

    return (
      <Nav className="ml-auto" navbar>
        <Dropdown nav isOpen={IsDropdownOpen} toggle={this.handleToggleDropdown}>
          <DropdownToggle nav caret>
            Dropdown
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItemLink to="/secure">
              Secure
            </DropdownItemLink>
            {
              !isAuthenticated &&
              <DropdownItemLink to="/login">
                Login
              </DropdownItemLink>
            }
            {
              !isAuthenticated &&
              <DropdownItemLink to="/register">
                Register
              </DropdownItemLink>
            }
          </DropdownMenu>
        </Dropdown>
        {
          isAuthenticated &&
            <NavItem>
              <NavLink to="/logout">
                <Icon name="sign-out" />
              </NavLink>
            </NavItem>
        }
      </Nav>
    )
  }
}

SecondaryNavigation.propTypes = {
  handleOpenDropdown: PropTypes.func.isRequired,
  handleCloseDropdown: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
  IsDropdownOpen: PropTypes.bool,
}

function mapDispatchToProps (dispatch) {
  return {
    handleOpenDropdown: () => {
      dispatch(openDropdown())
    },
    handleCloseDropdown: () => {
      dispatch(closeDropdown())
    },
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SecondaryNavigation))
