/* eslint-disable import/prefer-default-export */
import { createStructuredSelector } from 'reselect'
import { selectIsAuthenticated } from 'modules/auth/selectors'
import { selectIsDropdownOpen } from 'modules/app/selectors'

const mapStateToProps = createStructuredSelector({
  isAuthenticated: selectIsAuthenticated,
  IsDropdownOpen: selectIsDropdownOpen,
})

export {
  mapStateToProps,
}
