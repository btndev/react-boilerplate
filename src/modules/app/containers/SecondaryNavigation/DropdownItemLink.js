import React from 'react'
import { Link } from 'react-router-dom'
import { DropdownItem } from 'reactstrap'

function DropdownItemLink (props) {
  return (
    <DropdownItem tag={Link} {...props} />
  )
}

DropdownItemLink.propTypes = {
  ...DropdownItem.propTypes,
  ...Link.propTypes,
}

export default DropdownItemLink
