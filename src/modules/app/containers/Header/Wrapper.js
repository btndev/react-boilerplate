import styled from 'styled-components'
import PropTypes from 'prop-types'

const Wrapper = styled.header`
  display: block;
  ${(props) => (props.sticky && `
    position: sticky;
    top: 0;
    z-index: 1;  
  `)}
`

Wrapper.propTypes = {
  sticky: PropTypes.bool,
}

export default Wrapper
