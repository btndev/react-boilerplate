/* eslint-disable import/max-dependencies */
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
} from 'reactstrap'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
import MainNavigation from 'modules/app/containers/MainNavigation'
import SecondaryNavigation from 'modules/app/containers/SecondaryNavigation'

import { openNavbar, closeNavbar } from 'modules/app/action'

import Wrapper from './Wrapper'
import { mapStateToProps } from './selectors'

class Header extends PureComponent {
  handleToggleNavbar = () => {
    const {
      isNavbarOpen,
      handleOpenNavbar,
      handleCloseNavbar,
    } = this.props

    isNavbarOpen ? handleCloseNavbar() : handleOpenNavbar()
  }

  render () {
    const {
      isNavbarOpen,
    } = this.props

    return (
      <Wrapper sticky>
        <Navbar color="primary" dark expand="md" sticky="top">
          <NavbarBrand tag={Link} to="/">React boilerplate</NavbarBrand>
          <NavbarToggler onClick={this.handleToggleNavbar} />
          <Collapse isOpen={isNavbarOpen} navbar>
            <MainNavigation />
            <SecondaryNavigation />
          </Collapse>
        </Navbar>
      </Wrapper>
    )
  }
}

Header.propTypes = {
  isNavbarOpen: PropTypes.bool.isRequired,
  handleOpenNavbar: PropTypes.func.isRequired,
  handleCloseNavbar: PropTypes.func.isRequired,
}

function mapDispatchToProps (dispatch) {
  return {
    handleOpenNavbar: () => {
      dispatch(openNavbar())
    },
    handleCloseNavbar: () => {
      dispatch(closeNavbar())
    },
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header))
