/* eslint-disable import/prefer-default-export */
import { createStructuredSelector } from 'reselect'
import { selectIsNavbarOpen } from 'modules/app/selectors'

const mapStateToProps = createStructuredSelector({
  isNavbarOpen: selectIsNavbarOpen,
})

export {
  mapStateToProps,
}
