import { fromJS } from 'immutable'
import {
  OPEN_NAVBAR,
  CLOSE_NAVBAR,
  OPEN_DROPDOWN,
  CLOSE_DROPDOWN,
} from './constants'

const initialState = fromJS({
  isNavbarOpen: false,
  isDropdownOpen: false,
})

export default function reducer (state = initialState, action) {
  switch (action.type) {
    case OPEN_NAVBAR:
      return state.set('isNavbarOpen', true)
    case CLOSE_NAVBAR:
      return state.set('isNavbarOpen', false)
    case OPEN_DROPDOWN:
      return state.set('isDropdownOpen', true)
    case CLOSE_DROPDOWN:
      return state.set('isDropdownOpen', false)
    default:
      return state
  }
}
