/* eslint-disable import/prefer-default-export */

export const OPEN_NAVBAR = 'layout/OPEN_NAVBAR'
export const CLOSE_NAVBAR = 'layout/CLOSE_NAVBAR'

export const OPEN_DROPDOWN = 'layout/OPEN_DROPDOWN'
export const CLOSE_DROPDOWN = 'layout/CLOSE_DROPDOWN'
