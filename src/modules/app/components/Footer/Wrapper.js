import styled from 'styled-components'
import {
  footerHeight,
  gutterWidth,
} from 'styles/variables'

export default styled.footer`
  display: block;
  position: absolute;
  bottom: 0;
  width: 100%;
  background-color: #f5f5f5;
  text-align: center;
  height: ${footerHeight};
  line-height: ${footerHeight};
  padding-left: ${gutterWidth};
  padding-right: ${gutterWidth};
  box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.1);
`
