import React, { Component } from 'react'

import Wrapper from './Wrapper'

class Footer extends Component {
  shouldComponentUpdate () {
    // If rerender of specific component will never change it's content you can explicitly block it
    return false
  }

  render () {
    return (
      <Wrapper>
        Footer
      </Wrapper>
    )
  }
}

export default Footer
