import React from 'react'
import PropTypes from 'prop-types'
import { BreadcrumbItem } from 'reactstrap'
import Icon from 'components/Icon'

function FinalItem ({ icon, children }) {
  return (
    <BreadcrumbItem>
      {icon && <Icon name={icon} />}
      {children}
    </BreadcrumbItem>
  )
}

FinalItem.propTypes = {
  icon: PropTypes.string,
  children: PropTypes.node,
}

export default FinalItem
