import React from 'react'
import { Breadcrumbs as BaseBreadcrumbs } from 'react-breadcrumbs-dynamic'
import { Breadcrumb } from 'reactstrap'

import Item from './Item'
import FinalItem from './FinalItem'

function Breadcrumbs () {
  return (
    <BaseBreadcrumbs
      container={Breadcrumb}
      item={Item}
      finalItem={FinalItem}
    />
  )
}

export default Breadcrumbs
