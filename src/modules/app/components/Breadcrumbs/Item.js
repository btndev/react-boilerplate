import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { BreadcrumbItem } from 'reactstrap'
import Icon from 'components/Icon'

function Item ({ to, icon, children }) {
  return (
    <BreadcrumbItem>
      <NavLink to={to} >
        {icon && <Icon name={icon} />}
        {children}
      </NavLink>
    </BreadcrumbItem>
  )
}

Item.propTypes = {
  to: PropTypes.string.isRequired,
  icon: PropTypes.string,
  children: PropTypes.node,
}

export default Item
