import styled from 'styled-components'
import { footerHeight } from 'styles/variables'

const Container = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  padding-bottom: ${footerHeight};
`

export default Container
