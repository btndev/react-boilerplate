/* eslint-disable import/prefer-default-export */
import {
  OPEN_NAVBAR,
  CLOSE_NAVBAR,
  OPEN_DROPDOWN,
  CLOSE_DROPDOWN,
} from './constants'

function openNavbar () {
  return {
    type: OPEN_NAVBAR,
  }
}

function closeNavbar () {
  return {
    type: CLOSE_NAVBAR,
  }
}

function openDropdown () {
  return {
    type: OPEN_DROPDOWN,
  }
}

function closeDropdown () {
  return {
    type: CLOSE_DROPDOWN,
  }
}

export {
  openNavbar,
  closeNavbar,
  openDropdown,
  closeDropdown,
}
