import { reducer as formReducer } from 'redux-form/immutable'
import { loadingBarReducer } from 'react-redux-loading-bar'
import { reducer as toastrReducer } from 'react-redux-toastr'
import routerReducer from 'modules/router/reducer'
import authReducer from 'modules/auth/reducer'
import appReducer from 'modules/app/reducer'
// import your global reducers here

const reducers = {
  form: formReducer,
  loadingBar: loadingBarReducer,
  toastr: toastrReducer,
  router: routerReducer,
  auth: authReducer,
  app: appReducer,
  // add your global reducers here
}

export default reducers
