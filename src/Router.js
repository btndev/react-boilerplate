/* eslint-disable import/max-dependencies */
import React from 'react'
import { Route, Switch } from 'react-router'

import BreadcrumbRoute from 'modules/router/components/BreadcrumbRoute'

import LoginPage from 'pages/LoginPage'
import RegisterPage from 'pages/RegisterPage'
import SecurePage from 'pages/SecurePage'
import LogoutPage from 'pages/LogoutPage'
import HomePage from 'pages/HomePage'
import UsersPage from 'pages/UsersPage'
import NotFoundPage from 'pages/NotFoundPage'
import CounterPage from 'pages/CounterPage'

function Router () {
  return (
    <Switch>
      <BreadcrumbRoute path="/users" label="Users" component={UsersPage} />
      <BreadcrumbRoute path="/login" label="Login" component={LoginPage} />
      <BreadcrumbRoute path="/register" label="Register" component={RegisterPage} />
      <BreadcrumbRoute path="/secure" label="Secure" component={SecurePage} />
      <Route path="/logout" component={LogoutPage} />
      <BreadcrumbRoute path="/counter" label="Counter" component={CounterPage} />
      <Route exact path="/" component={HomePage} />
      <Route path="*" component={NotFoundPage} />
    </Switch>
  )
}

export default Router
