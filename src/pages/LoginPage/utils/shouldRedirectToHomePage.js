import PropTypes from 'prop-types'
import extractRedirectUrl from 'modules/auth/utils/extractRedirectUrl'

function shouldRedirectToHomePage (props) {
  const { success } = props

  if (!success) {
    return false
  }

  const { location: { search } } = props

  const redirectUrl = extractRedirectUrl(search)
  if (redirectUrl) {
    return false
  }

  return true
}

shouldRedirectToHomePage.propTypes = {
  location: PropTypes.shape({
    search: PropTypes.string,
  }),
  success: PropTypes.bool,
}

export default shouldRedirectToHomePage
