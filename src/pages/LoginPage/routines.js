/* eslint-disable import/prefer-default-export */
import { createRoutine } from 'modules/routine/utils'
import { LOGIN } from './constants'

export const loginRoutine = createRoutine(LOGIN)
