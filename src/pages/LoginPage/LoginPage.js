/* eslint-disable import/max-dependencies */
import React, { PureComponent } from 'react'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import { connectPage } from 'modules/page'
import MiddleFormLayout from 'layouts/MiddleFormLayout'
import Success from 'components/Success'

import shouldRedirectToHomePage from './utils/shouldRedirectToHomePage'
import reducer from './reducer'
import saga from './saga'
import {
  PAGE_KEY,
} from './constants'
import LoginForm from './LoginForm'
import { login } from './actions'
import { mapStateToProps } from './selectors'

class LoginPage extends PureComponent {
  componentWillUpdate (nextProps) {
    this.redirectIfNeeded(nextProps)
  }

  redirectIfNeeded (refProps = this.props) {
    if (!shouldRedirectToHomePage(refProps)) {
      return
    }

    const { history: { push } } = this.props
    push('/')
  }

  render () {
    const { success, handleSubmit } = this.props

    return (
      <MiddleFormLayout headline="Login">
        {success
          ? <Success alert>Redirecting...</Success>
          : <LoginForm onSubmit={handleSubmit} />
        }
      </MiddleFormLayout>
    )
  }
}

LoginPage.propTypes = {
  success: PropTypes.bool,
  handleSubmit: PropTypes.func,
  history: PropTypes.shape({
    push: PropTypes.func,
  }),
}

const mapDispatchToProps = (dispatch) => ({
  handleSubmit: bindActionCreators(login, dispatch),
})

export default connectPage({
  key: PAGE_KEY,
  reducer,
  saga,
  mapStateToProps,
  mapDispatchToProps,
  withRouter: true,
})(LoginPage)
