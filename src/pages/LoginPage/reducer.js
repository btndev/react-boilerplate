import { createRoutineReducer } from 'modules/routine'
import { loginRoutine } from './routines'

export default createRoutineReducer(loginRoutine)
