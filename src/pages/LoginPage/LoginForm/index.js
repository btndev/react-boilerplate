import React from 'react'
import PropTypes from 'prop-types'
import form from 'modules/form/hoc/form'
import Form from 'modules/form/components/Form'
import FormRowField from 'modules/form/components/FormRowField'

import schema from './schema'

function LoginForm (props) {
  const { handleSubmit } = props

  return (
    <Form {...props} onSubmit={handleSubmit} submitLabel="Login" >
      <FormRowField name="username" type="text" label="Email" />
      <FormRowField name="password" type="password" label="Password" />
    </Form>
  )
}

LoginForm.propTypes = {
  handleSubmit: PropTypes.func,
}

export default form({
  schema,
  form: 'login',
})(LoginForm)
