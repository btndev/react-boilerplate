import { call, put, takeLatest } from 'redux-saga/effects'
import api from 'services/api'
import { setAuthToken } from 'modules/auth/actions'
import createFormSubmissionError from 'modules/form/utils/createFormSubmissionError'
import { loginRoutine } from './routines'

function * loginFormSaga (action) {
  try {
    yield put(loginRoutine.request())
    const { data } = yield call(api.post, '/auth/login', action.payload.values)
    if (data.token) {
      yield put(setAuthToken(data.token))
    }
    yield put(loginRoutine.success(data))
  } catch (error) {
    yield put(loginRoutine.failure(createFormSubmissionError(error)))
  } finally {
    yield put(loginRoutine.fulfill())
  }
}

function * saga () {
  yield takeLatest(loginRoutine.TRIGGER, loginFormSaga)
}

export default saga
