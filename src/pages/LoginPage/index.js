import { asyncPage } from 'modules/page'

export default asyncPage({
  loader: () => import(/* webpackChunkName: 'login-page' */'./LoginPage'),
})
