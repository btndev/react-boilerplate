/* eslint-disable import/prefer-default-export */
import { routineToReduxFormAction } from 'modules/routine'
import { loginRoutine } from './routines'

export const login = routineToReduxFormAction(loginRoutine)
