import React from 'react'
import DefaultLayout from 'layouts/DefaultLayout'

function NotFoundPage () {
  return (
    <DefaultLayout headline="Not Found">
      Page was not found
    </DefaultLayout>
  )
}

export default NotFoundPage
