import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { connectPage } from 'modules/page'
import { logout } from 'modules/auth/actions'

class LogoutPage extends Component {
  componentWillMount () {
    this.props.logout()
  }

  shouldComponentUpdate () {
    return true
  }

  render () {
    return (<Redirect to="/" />)
  }
}

const mapDispatchToProps = {
  logout,
}

LogoutPage.propTypes = {
  logout: PropTypes.func,
}

export default connectPage({
  mapDispatchToProps,
})(LogoutPage)
