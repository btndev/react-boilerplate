import {
  INCREMENT_COUNTER,
  DECREMENT_COUNTER,
} from './constants'

function incrementCounter () {
  return {
    type: INCREMENT_COUNTER,
  }
}

function decrementCounter () {
  return {
    type: DECREMENT_COUNTER,
  }
}

export {
  incrementCounter,
  decrementCounter,
}
