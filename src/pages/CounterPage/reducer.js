import { fromJS } from 'immutable'

import {
  INCREMENT_COUNTER,
  DECREMENT_COUNTER,
} from './constants'

const initialState = fromJS({
  counter: 0,
})

function reducer (state = initialState, action) {
  switch (action.type) {
    case INCREMENT_COUNTER:
      return state.merge({
        counter: state.get('counter') + 1,
      })
    case DECREMENT_COUNTER:
      return state.merge({
        counter: state.get('counter') - 1,
      })
    default:
      return state
  }
}

export default reducer
