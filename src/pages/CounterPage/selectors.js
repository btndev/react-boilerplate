/* eslint-disable import/prefer-default-export */
import { createSelector, createStructuredSelector } from 'reselect'
import { PAGE_KEY } from './constants'

/*
 * Selector to select main page state from Redux state.
 */
const selectCounterPageState = (state) => (
  state.get(PAGE_KEY)
)

/*
 * Select counter value from counter page reducer.
 * This is equivalent with:
 *
 * const selectCounter = (state, props) => {
 *    return state.get(PAGE_KEY).get('counter')
 * }
 */
const selectCounter = createSelector(
  selectCounterPageState,
  (simplePageState) => (simplePageState.get('counter'))
)

/*
 * Create map state to props for Simple Page.
 * This is equivalent with:
 *
 * const mapStateToProps = (state, props) => {
 *   return {
 *      counter: selectCounter(state, props),
 *   }
 * }
 */
const mapStateToProps = createStructuredSelector({
  counter: selectCounter,
})

export {
  mapStateToProps,
}
