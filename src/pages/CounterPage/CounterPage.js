import React from 'react'
import PropTypes from 'prop-types'
import DefaultLayout from 'layouts/DefaultLayout'
import { connectPage } from 'modules/page'
import Button from 'components/Button'

import { PAGE_KEY } from './constants'
import {
  incrementCounter,
  decrementCounter,
} from './actions'
import reducer from './reducer'
import { mapStateToProps } from './selectors'
import Counter from './Counter'

function CounterPage ({ counter, handleIncrementCounter, handleDecrementCounter }) {
  return (
    <DefaultLayout headline="Simple counter page" >
      <Counter>
        Counter: {counter}
      </Counter>
      <Button onClick={handleIncrementCounter}>Increment counter</Button>
      {` `}
      <Button onClick={handleDecrementCounter}>Decrement counter</Button>
    </DefaultLayout>
  )
}

CounterPage.propTypes = {
  handleIncrementCounter: PropTypes.func.isRequired,
  handleDecrementCounter: PropTypes.func.isRequired,
  counter: PropTypes.number,
}

function mapDispatchToProps (dispatch) {
  return {
    handleIncrementCounter: () => {
      dispatch(incrementCounter())
    },
    handleDecrementCounter: () => {
      dispatch(decrementCounter())
    },
  }
}

/*
 * Here reducer is injected using PAGE_KEY for Redux state key.
 * It will also connect component to Redux state using connect
 */
export default connectPage({
  key: PAGE_KEY,
  reducer,
  mapStateToProps,
  mapDispatchToProps,
})(CounterPage)
