import { asyncPage } from 'modules/page'

/*
 * Here CounterPage React Component is being wrapped using asyncPage HOC to utilize webpack code splitting (https://webpack.js.org/guides/code-splitting/).
 * Webpack will generate separate chunk for ./CounterPage.js and all of his dependencies
 * webpackChunkName is to explicitly specify how this chunk should be called.
 */
export default asyncPage({
  loader: () => import(/* webpackChunkName: 'simple-page' */'./CounterPage'),
})
