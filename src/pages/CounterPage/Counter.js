import styled from 'styled-components'

const Counter = styled.div`
  font-size: 2rem;
`

export default Counter
