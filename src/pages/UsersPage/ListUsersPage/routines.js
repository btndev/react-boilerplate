/* eslint-disable import/prefer-default-export */
import { createRoutine } from 'modules/routine/utils'
import { FETCH_USERS } from './constants'

export const fetchUsersRoutine = createRoutine(FETCH_USERS)
