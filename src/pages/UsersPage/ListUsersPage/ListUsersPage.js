/* eslint-disable import/max-dependencies */
import React, { PureComponent } from 'react'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'

import { connectPage } from 'modules/page'
import DefaultLayout from 'layouts/DefaultLayout'
import AddUserButton from 'modules/user/components/AddUserButton'
import UserList from 'modules/user/components/UserList'

import reducer from './reducer'
import saga from './saga'
import { PAGE_KEY } from './constants'
import { fetchUsers } from './actions'
import { mapStateToProps } from './selectors'

class ListUsersPage extends PureComponent {
  componentDidMount () {
    const { fetchData } = this.props

    fetchData()
  }

  render () {
    const { data } = this.props

    return (
      <DefaultLayout
        headline="Users"
        actions={<AddUserButton />}
      >
        <UserList users={data} />
      </DefaultLayout>
    )
  }
}

ListUsersPage.propTypes = {
  data: ImmutablePropTypes.list,
  fetchData: PropTypes.func,
}

const mapDispatchToProps = (dispatch) => ({
  fetchData: bindActionCreators(fetchUsers, dispatch),
})

export default connectPage({
  key: PAGE_KEY,
  reducer,
  saga,
  mapStateToProps,
  mapDispatchToProps,
})(ListUsersPage)
