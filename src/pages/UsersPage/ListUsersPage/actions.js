/* eslint-disable import/prefer-default-export */
import { routineToAction } from 'modules/routine'
import { fetchUsersRoutine } from './routines'

export const fetchUsers = routineToAction(fetchUsersRoutine)
