import { createRoutineReducer } from 'modules/routine'
import { fetchUsersRoutine } from './routines'

export default createRoutineReducer(fetchUsersRoutine)
