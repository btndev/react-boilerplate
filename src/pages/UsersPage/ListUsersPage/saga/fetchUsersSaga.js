import { call, put, takeLatest } from 'redux-saga/effects'
import api from 'services/api'
import { withMinDelay } from 'utils/saga'
import extractErrorMessage from 'modules/api/utils/extractErrorMessage'
import { fetchUsersRoutine } from 'pages/UsersPage/ListUsersPage/routines'

function * fetchUsers () {
  try {
    yield put(fetchUsersRoutine.request())
    const { data } = yield withMinDelay(call(api.get, '/users'))
    yield put(fetchUsersRoutine.success(data))
  } catch (error) {
    yield put(fetchUsersRoutine.failure(extractErrorMessage(error)))
  } finally {
    yield put(fetchUsersRoutine.fulfill())
  }
}

function * fetchUsersSaga () {
  yield takeLatest(fetchUsersRoutine.TRIGGER, fetchUsers)
}

export default fetchUsersSaga
