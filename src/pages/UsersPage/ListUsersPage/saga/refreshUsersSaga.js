import { delay } from 'redux-saga'
import { put, takeLatest } from 'redux-saga/effects'
import { deleteUserRoutine } from 'modules/user/routines'
import { fetchUsersRoutine } from 'pages/UsersPage/ListUsersPage/routines'

function * refreshUsers () {
  yield delay(100)
  yield put(fetchUsersRoutine.trigger())
}

function * refreshUsersSaga () {
  yield takeLatest(deleteUserRoutine.FULFILL, refreshUsers)
}

export default refreshUsersSaga
