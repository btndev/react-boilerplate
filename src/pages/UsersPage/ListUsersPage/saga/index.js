import { fork, all } from 'redux-saga/effects'
import fetchUsersSaga from './fetchUsersSaga'
import refreshUsersSaga from './refreshUsersSaga'

function * saga () {
  yield all([
    fork(fetchUsersSaga),
    fork(refreshUsersSaga),
  ])
}

export default saga
