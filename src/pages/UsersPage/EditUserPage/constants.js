export const PAGE_KEY = 'editUserPage'
export const FETCH_USER = `${PAGE_KEY}/FETCH_USER`
export const UPDATE_USER = `${PAGE_KEY}/UPDATE_USER`
