import { call, put, takeLatest } from 'redux-saga/effects'
import api from 'services/api'
import { withMinDelay } from 'utils/saga'
import extractErrorMessage from 'modules/api/utils/extractErrorMessage'
import { fetchUserRoutine } from '../routines'

function * fetchUser ({ payload }) {
  try {
    const { userId } = payload
    yield put(fetchUserRoutine.request())
    const { data } = yield withMinDelay(call(api.get, `/users/${userId}`))
    yield put(fetchUserRoutine.success(data))
  } catch (error) {
    yield put(fetchUserRoutine.failure(extractErrorMessage(error)))
  } finally {
    yield put(fetchUserRoutine.fulfill())
  }
}

function * fetchUserSaga () {
  return yield takeLatest(fetchUserRoutine.TRIGGER, fetchUser)
}

export default fetchUserSaga
