import { fork, all } from 'redux-saga/effects'
import fetchDataSaga from './fetchUserSaga'
import updateUserSaga from './updateUserSaga'

function * saga () {
  yield all([
    fork(fetchDataSaga),
    fork(updateUserSaga),
  ])
}

export default saga
