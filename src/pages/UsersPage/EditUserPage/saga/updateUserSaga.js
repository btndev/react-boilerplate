import { call, put, takeLatest } from 'redux-saga/effects'
// import { initialize } from 'redux-form/immutable'
import api from 'services/api'
import { withMinDelay } from 'utils/saga'
import { pick } from 'utils/immutable'
// import { FORM_NAME } from 'modules/user/forms/EditUserForm/constants'
import createFormSubmissionError from 'modules/form/utils/createFormSubmissionError'
import { displaySuccessToast } from 'modules/toast/actions'
import {
  updateUserRoutine,
  fetchUserRoutine,
} from '../routines'

function * resetFormDirtyState (values) {
  // more advanced way, requires changes selector to prioritize form state over route state
  // yield put(initialize(FORM_NAME, values.toMap()))

  // simpler form, but not recommended
  yield put(fetchUserRoutine.success(values))
}

function * updateUser ({ payload }) {
  try {
    const { values } = payload
    const itemId = values.get('id')
    const data = pick(values, 'firstName', 'lastName') // pick only keys that api allows
    yield put(updateUserRoutine.request())
    yield withMinDelay(call(api.patch, `/users/${itemId}`, data))
    yield put(updateUserRoutine.success())
    yield put(displaySuccessToast('User updated'))
    yield resetFormDirtyState(values)
  } catch (error) {
    yield put(updateUserRoutine.failure(createFormSubmissionError(error)))
  } finally {
    yield put(updateUserRoutine.fulfill())
  }
}

function * updateUserSaga () {
  yield takeLatest(updateUserRoutine.TRIGGER, updateUser)
}

export default updateUserSaga
