/* eslint-disable import/prefer-default-export */
import { routineToAction, routineToReduxFormAction } from 'modules/routine'
import {
  fetchUserRoutine,
  updateUserRoutine,
} from './routines'

export const fetchUser = routineToAction(fetchUserRoutine)

export const updateUser = routineToReduxFormAction(updateUserRoutine)
