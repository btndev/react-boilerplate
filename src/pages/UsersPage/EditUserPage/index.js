import { asyncPage } from 'modules/page'

export default asyncPage({
  loader: () => import(/* webpackChunkName: 'edit-user-page' */'./EditUserPage'),
})
