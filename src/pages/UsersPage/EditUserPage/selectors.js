/* eslint-disable import/prefer-default-export */
// import { FORM_NAME } from 'modules/user/forms/EditUserForm/constants'
// import { getFormInitialValues } from 'redux-form/immutable'

import { createPageSelectors } from 'modules/page'
import { PAGE_KEY } from './constants'

const { mapStateToProps: mapStateToPropsForFetchPage } = createPageSelectors(PAGE_KEY, 'fetch')

// const selectFormInitialValues = getFormInitialValues(FORM_NAME)

function mapStateToProps (...args) {
  const props = mapStateToPropsForFetchPage(...args)

  // override data prop with form initial values to allow reset state via action in saga after successful update
  // required for more advanced way of resetting form state in updateUserSaga
  // const formInitialValues = selectFormInitialValues(...args)
  // if (formInitialValues) {
  //   return {
  //     ...props,
  //     data: formInitialValues,
  //   }
  // }

  return props
}

export {
  mapStateToProps,
}
