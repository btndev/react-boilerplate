import { createStructuredRoutineReducer } from 'modules/routine'
import { fetchUserRoutine, updateUserRoutine } from './routines'

export default createStructuredRoutineReducer({
  fetch: fetchUserRoutine,
  update: updateUserRoutine,
})
