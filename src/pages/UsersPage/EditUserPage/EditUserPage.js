/* eslint-disable import/max-dependencies */
import React, { PureComponent } from 'react'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import ImmutablePropTypes from 'react-immutable-proptypes'
import { connectPage } from 'modules/page'
import DefaultLayout from 'layouts/DefaultLayout'
import EditUserForm from 'modules/user/forms/EditUserForm'
import AddUserButton from 'modules/user/components/AddUserButton'

import reducer from './reducer'
import saga from './saga'
import { PAGE_KEY } from './constants'
import { fetchUser, updateUser } from './actions'
import { mapStateToProps } from './selectors'

class EditUserPage extends PureComponent {
  componentDidMount () {
    const { fetchData, match: { params } } = this.props

    fetchData(params)
  }

  render () {
    const { handleSubmit, data } = this.props

    return (
      <DefaultLayout
        headline="Edit user"
        actions={<AddUserButton />}
      >
        <EditUserForm
          onSubmit={handleSubmit}
          initialValues={data}
          successMessage="User updated"
          submitLabel="Update"
        />
      </DefaultLayout>
    )
  }
}

EditUserPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      userId: PropTypes.string.isRequired,
    }),
  }).isRequired,
  data: ImmutablePropTypes.map,
  fetchData: PropTypes.func,
  handleSubmit: PropTypes.func,
}

const mapDispatchToProps = (dispatch) => ({
  fetchData: bindActionCreators(fetchUser, dispatch),
  handleSubmit: bindActionCreators(updateUser, dispatch),
})

export default connectPage({
  key: PAGE_KEY,
  reducer,
  saga,
  mapStateToProps,
  mapDispatchToProps,
})(EditUserPage)
