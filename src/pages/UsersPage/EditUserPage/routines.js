/* eslint-disable import/prefer-default-export */
import { createRoutine } from 'modules/routine'
import {
  FETCH_USER,
  UPDATE_USER,
} from './constants'

export const fetchUserRoutine = createRoutine(FETCH_USER)

export const updateUserRoutine = createRoutine(UPDATE_USER)
