/* eslint-disable import/prefer-default-export */
import { routineToReduxFormAction } from 'modules/routine'
import { createUserRoutine } from './routines'

export const createUser = routineToReduxFormAction(createUserRoutine)
