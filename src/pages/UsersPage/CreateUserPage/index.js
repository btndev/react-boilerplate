import { asyncPage } from 'modules/page'

export default asyncPage({
  loader: () => import(/* webpackChunkName: 'create-user-page' */'./CreateUserPage'),
})
