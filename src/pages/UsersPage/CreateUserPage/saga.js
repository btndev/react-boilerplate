import { call, put, takeLatest } from 'redux-saga/effects'
import { push } from 'react-router-redux'
import { reset } from 'redux-form/immutable'
import api from 'services/api'
import { withMinDelay } from 'utils/saga'
import createFormSubmissionError from 'modules/form/utils/createFormSubmissionError'
import { FORM_NAME } from 'modules/user/forms/CreateUserForm/constants'
import { displaySuccessToast } from 'modules/toast/actions'
import { createUserRoutine } from './routines'

function * createUserSaga ({ payload }) {
  try {
    const { values } = payload
    yield put(createUserRoutine.request())
    const { headers: { id } } = yield withMinDelay(call(api.post, `/users`, values))
    yield put(createUserRoutine.success())
    yield put(reset(FORM_NAME))
    yield put(displaySuccessToast('User created'))
    yield put(push(`/users/${id}`))
  } catch (error) {
    yield put(createUserRoutine.failure(createFormSubmissionError(error)))
  } finally {
    yield put(createUserRoutine.fulfill())
  }
}

function * saga () {
  yield takeLatest(createUserRoutine.TRIGGER, createUserSaga)
}

export default saga
