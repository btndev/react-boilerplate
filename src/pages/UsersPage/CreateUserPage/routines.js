/* eslint-disable import/prefer-default-export */
import { createRoutine } from 'modules/routine'
import {
  CREATE_USER,
} from './constants'

export const createUserRoutine = createRoutine(CREATE_USER)
