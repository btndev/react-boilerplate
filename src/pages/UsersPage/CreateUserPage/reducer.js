import { createRoutineReducer } from 'modules/routine'
import { createUserRoutine } from './routines'

export default createRoutineReducer(createUserRoutine)
