import React, { PureComponent } from 'react'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types'
import { connectPage } from 'modules/page'
import DefaultLayout from 'layouts/DefaultLayout'
import CreateUserForm from 'modules/user/forms/CreateUserForm'

import reducer from './reducer'
import saga from './saga'
import { PAGE_KEY } from './constants'
import { createUser } from './actions'

class CreateUserPage extends PureComponent {
  render () {
    const { handleSubmit } = this.props

    return (
      <DefaultLayout headline="New user">
        <CreateUserForm
          onSubmit={handleSubmit}
          submitLabel="Create"
        />
      </DefaultLayout>
    )
  }
}

CreateUserPage.propTypes = {
  handleSubmit: PropTypes.func,
}

const mapDispatchToProps = (dispatch) => ({
  handleSubmit: bindActionCreators(createUser, dispatch),
})

export default connectPage({
  key: PAGE_KEY,
  reducer,
  saga,
  mapDispatchToProps,
})(CreateUserPage)
