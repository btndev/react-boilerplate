/* eslint-disable import/prefer-default-export */
import { createPageSelectors } from 'modules/page'
import { PAGE_KEY } from './constants'

const { mapStateToProps } = createPageSelectors(PAGE_KEY)

export {
  mapStateToProps,
}
