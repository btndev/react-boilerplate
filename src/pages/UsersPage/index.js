import React from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router'
import BreadcrumbRoute from 'modules/router/components/BreadcrumbRoute'
import userIsAuthenticated from 'modules/auth/hoc/userIsAuthenticated'

import ListUsersPage from './ListUsersPage'
import CreateUserPage from './CreateUserPage'
import EditUserPage from './EditUserPage'

function UsersPage ({ match }) {
  return (
    <Switch>
      <Route exact path={`${match.url}`} component={ListUsersPage} />
      <BreadcrumbRoute label="Add user" path={`${match.url}/new`} component={CreateUserPage} />
      <BreadcrumbRoute label="Edit user" path={`${match.url}/:userId`} component={EditUserPage} />
    </Switch>
  )
}

UsersPage.propTypes = {
  match: PropTypes.shape({
    url: PropTypes.string,
  }),
}

export default userIsAuthenticated(UsersPage)
