import React from 'react'
import DefaultLayout from 'layouts/DefaultLayout'

function HomePage () {
  return (
    <DefaultLayout headline="Home">
      Welcome to home page
    </DefaultLayout>
  )
}

export default HomePage
