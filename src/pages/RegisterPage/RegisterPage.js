/* eslint-disable import/max-dependencies */
import React from 'react'
import { bindActionCreators } from 'redux'
import { connectPage } from 'modules/page'
import MiddleFormLayout from 'layouts/MiddleFormLayout'
import Success from 'components/Success'

import PropTypes from 'prop-types'
import reducer from './reducer'
import saga from './saga'
import {
  PAGE_KEY,
} from './constants'
import RegisterForm from './RegisterForm'
import { register } from './actions'
import { mapStateToProps } from './selectors'

function RegisterPage ({ success, handleSubmit }) {
  return (
    <MiddleFormLayout headline="Register">
      {success
        ? <Success alert>Account has been created</Success>
        : <RegisterForm onSubmit={handleSubmit} />
      }
    </MiddleFormLayout>
  )
}

RegisterPage.propTypes = {
  success: PropTypes.bool,
  handleSubmit: PropTypes.func,
}

const mapDispatchToProps = (dispatch) => ({
  handleSubmit: bindActionCreators(register, dispatch),
})

export default connectPage({
  key: PAGE_KEY,
  reducer,
  saga,
  mapStateToProps,
  mapDispatchToProps,
})(RegisterPage)
