/* eslint-disable import/prefer-default-export */
import { createRoutine } from 'modules/routine/utils'
import { REGISTER } from './constants'

export const registerRoutine = createRoutine(REGISTER)
