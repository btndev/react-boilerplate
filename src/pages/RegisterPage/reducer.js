import { createRoutineReducer } from 'modules/routine'
import { registerRoutine } from './routines'

export default createRoutineReducer(registerRoutine)
