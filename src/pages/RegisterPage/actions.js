/* eslint-disable import/prefer-default-export */
import { routineToReduxFormAction } from 'modules/routine'
import { registerRoutine } from './routines'

export const register = routineToReduxFormAction(registerRoutine)
