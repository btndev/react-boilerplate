
const schema = {
  type: 'object',
  properties: {
    username: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 6,
    },
  },
  required: ['username', 'password'],
}

export default schema
