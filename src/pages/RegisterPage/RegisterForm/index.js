import React from 'react'
import PropTypes from 'prop-types'
import form from 'modules/form/hoc/form'
import Form from 'modules/form/components/Form'
import FormRowField from 'modules/form/components/FormRowField'

import schema from './schema'

function RegisterForm (props) {
  const { handleSubmit } = props

  return (
    <Form {...props} onSubmit={handleSubmit} submitLabel="Register" >
      <FormRowField type="email" name="username" label="Email" />
      <FormRowField type="password" name="password" label="Password" />
    </Form>
  )
}

RegisterForm.propTypes = {
  handleSubmit: PropTypes.func,
}

export default form({
  schema,
  form: 'register',
})(RegisterForm)
