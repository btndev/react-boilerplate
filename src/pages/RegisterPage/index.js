import { asyncPage } from 'modules/page'

export default asyncPage({
  loader: () => import(/* webpackChunkName: 'register-page' */'./RegisterPage'),
})
