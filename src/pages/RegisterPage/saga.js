import { call, put, takeLatest } from 'redux-saga/effects'
import api from 'services/api'
import { setAuthToken } from 'modules/auth/actions'
import createFormSubmissionError from 'modules/form/utils/createFormSubmissionError'
import { registerRoutine } from './routines'

function * registerFormSaga (action) {
  try {
    yield put(registerRoutine.request())
    const { data } = yield call(api.post, '/auth/register', action.payload.values)
    if (data && data.token) {
      yield put(setAuthToken(data.token))
    }
    yield put(registerRoutine.success(data))
  } catch (error) {
    yield put(registerRoutine.failure(createFormSubmissionError(error)))
  } finally {
    yield put(registerRoutine.fulfill())
  }
}

function * saga () {
  yield takeLatest(registerRoutine.TRIGGER, registerFormSaga)
}

export default saga
