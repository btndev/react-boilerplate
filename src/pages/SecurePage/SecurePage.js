import React from 'react'
import userIsAuthenticated from 'modules/auth/hoc/userIsAuthenticated'
import DefaultLayout from 'layouts/DefaultLayout'

function SecurePage () {
  return (
    <DefaultLayout headline="Secure" >
      Secure!!!!!!!
    </DefaultLayout>
  )
}

export default userIsAuthenticated(SecurePage)
