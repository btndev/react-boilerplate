import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { displayConfirmToast } from 'modules/toast/actions'

import PropTypes from 'prop-types'

class Confirm extends PureComponent {
  constructor (props) {
    super(props)

    this.handleClick = this.handleClick.bind(this)
  }

  handleClick (event) {
    if (event) {
      event.preventDefault()
    }

    const { dispatch, message, onConfirm, onCancel } = this.props

    dispatch(displayConfirmToast({
      message,
      onConfirm,
      onCancel,
    }))
  }

  render () {
    const { children } = this.props

    return React.cloneElement(children, {
      onClick: this.handleClick,
    })
  }
}

Confirm.propTypes = {
  dispatch: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  message: PropTypes.string.isRequired,
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
}

export default connect()(Confirm)
