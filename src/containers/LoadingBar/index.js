import { ImmutableLoadingBar as LoadingBar } from 'react-redux-loading-bar'
import styled from 'styled-components'

export default styled(LoadingBar)`
  position: fixed;
  top: 0;
  z-index: 2;
  height: 2px;
  background-color: #fff;
`
