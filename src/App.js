/* eslint-disable import/max-dependencies */
import React from 'react'
import { Helmet } from 'react-helmet'
import LoadingBar from 'containers/LoadingBar'
import ToastDisplay from 'modules/toast/containers/ToastDisplay'

import Wrapper from 'modules/app/components/Wrapper'
import Header from 'modules/app/containers/Header'
import Breadcrumbs from 'modules/app/components/Breadcrumbs'
import BreadcrumbsItem from 'modules/app/components/BreadcrumbsItem'
import Container from 'modules/app/components/Container'
import Footer from 'modules/app/components/Footer'

import AuthenticateUser from 'modules/auth/containers/AuthenticateUser'

import Router from './Router'

function App () {
  return (
    <Wrapper>
      <LoadingBar />
      <ToastDisplay />
      <AuthenticateUser />
      <Helmet
        titleTemplate="%s - Bitnoi.se react boilerplate"
        defaultTitle="Welcome"
      />
      <Header />
      <Container>
        <Breadcrumbs />
        <BreadcrumbsItem to="/" icon="home" />
        <Router />
      </Container>
      <Footer />
    </Wrapper>
  )
}

export default App
