import React from 'react'
import PropTypes from 'prop-types'
import Container from 'modules/layout/components/Container'
import Row from 'modules/layout/components/Row'
import Column from 'modules/layout/components/Column'
import BaseLayout from 'layouts/BaseLayout'
import Headline from 'modules/layout/components/Headline'

import ActionColumn from './ActionColumn'

function DefaultLayout (props) {
  const { headline, actions, children } = props

  return (
    <BaseLayout {...props} className="flex-row align-items-center">
      <Container>
        <Row>
          <Column md={6}>
            <Headline>
              {headline}
            </Headline>
          </Column>
          <ActionColumn md={6}>
            {actions}
          </ActionColumn>
        </Row>
        <Row className="mt-3">
          <Column md={12}>
            {children}
          </Column>
        </Row>
      </Container>
    </BaseLayout>
  )
}

DefaultLayout.propTypes = {
  ...BaseLayout.propTypes,
  headline: PropTypes.string,
  children: PropTypes.node,
  actions: PropTypes.node,
}

export default DefaultLayout
