import React from 'react'
import Column from 'modules/layout/components/Column'

function ActionColumn (props) {
  return (
    <Column {...props} className="justify-content-end text-right" />
  )
}

ActionColumn.propTypes = {
  ...Column.propTypes,
}

export default ActionColumn
