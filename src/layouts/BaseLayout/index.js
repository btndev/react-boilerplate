import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import isString from 'lodash/isString'

import Wrapper from './Wrapper'

function extractTitle (props) {
  if (props.title) {
    return props.title
  }

  if (props.headline && isString(props.headline)) {
    return props.headline
  }

  return undefined
}

function BaseLayout (props) {
  const title = extractTitle(props)

  const { children } = props

  return (
    <Wrapper>
      {title && <Helmet title={title} />}
      {children}
    </Wrapper>
  )
}

BaseLayout.propTypes = {
  title: PropTypes.string, // eslint-disable-line react/no-unused-prop-types
  headline: PropTypes.oneOfType([ // eslint-disable-line react/no-unused-prop-types
    PropTypes.string,
    PropTypes.node,
  ]),
  children: PropTypes.node,
}

export default BaseLayout
