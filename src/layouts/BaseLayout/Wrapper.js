import styled from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  padding-top: 15px;
  padding-bottom: 15px;
`

export default Wrapper
