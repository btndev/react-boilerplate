import React from 'react'
import PropTypes from 'prop-types'
import Column from 'modules/layout/components/Column'
import Container from 'modules/layout/components/Container'
import Row from 'modules/layout/components/Row'
import Card from 'components/Card'

import BaseLayout from 'layouts/BaseLayout'
import Headline from 'modules/layout/components/Headline'
import Middle from 'components/Middle'

function MiddleFormLayout (props) {
  const { headline, children } = props

  const header = (
    <Headline clearMargin>
      {headline}
    </Headline>
  )

  return (
    <BaseLayout {...props} >
      <Middle>
        <Container>
          <Row className="justify-content-md-center">
            <Column sm={12} md={6} >
              <Card header={header}>
                {children}
              </Card>
            </Column>
          </Row>
        </Container>
      </Middle>
    </BaseLayout>
  )
}

MiddleFormLayout.propTypes = {
  ...BaseLayout.propTypes,
  headline: PropTypes.string,
  children: PropTypes.node,
}

export default MiddleFormLayout
