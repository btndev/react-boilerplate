server {
  # Hide server information
  server_tokens off;

  listen 80;
  server_name http2 localhost;
  root /app;

  # Set real ip from proxy
  real_ip_header X-Forwarded-For;
  set_real_ip_from 172.17.0.1;

  # To enable SSL follow this instruction: https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04

  # Gzip
  gzip on;
  gzip_min_length  1100;
  gzip_buffers  4 32k;
  gzip_types
      text/css
      text/javascript
      text/xml
      text/plain
      application/javascript
      application/x-javascript
      application/json
      application/xml
      application/rss+xml
      application/atom+xml
      font/truetype
      font/opentype
      image/svg+xml;
  gzip_vary on;
  gzip_comp_level 6;

  # Redirect all requests for file that don't exist to index.html
  location / {
    try_files $uri /index.html;
  }

  # Don't cache index.html
  location /index.html {
    etag off;
    expires off;
    add_header Cache-Control 'no-store, no-cache';
  }

  # Cache manifest.json for 1 day
  location /manifest.json {
    etag off;
    expires 1d;
    add_header Cache-Control public;
  }

  # Disable cache service worker caching
  location /service-worker.js {
    etag off;
    add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
    proxy_cache_bypass $http_pragma;
    proxy_cache_revalidate on;
    expires off;
    access_log off;
  }

  # Set cache headers for static files
  location ~ ^/static/ {
    expires max;
    add_header Cache-Control public;
    access_log off;
  }

  # Proxy pass /api to node process
  # location /api/ {
  #   proxy_pass http://localhost:3000;
  #   proxy_http_version 1.1;
  #   proxy_set_header X-Forwarded-Proto https;
  #   proxy_set_header Upgrade $http_upgrade;
  #   proxy_set_header Connection 'upgrade';
  #   proxy_set_header Host $host;
  #   proxy_cache_bypass $http_upgrade;
  #
  # }
}
