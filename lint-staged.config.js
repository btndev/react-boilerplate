module.exports = {
  '**/*.{js,jsx}': 'lint:eslint',
  'src/**/*.js': 'lint:stylelint',
  'package.json': 'validate',
  'yarn.lock': 'validate',
}
